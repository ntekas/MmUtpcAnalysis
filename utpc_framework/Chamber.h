#ifndef CHAMBER_H_INCLUDED
#define CHAMBER_H_INCLUDED

#include <vector>
#include <string>
#include <iostream>

#include "TH1.h"
#include "TObject.h"
#include "TVector3.h"

#include "ChamberOutput.h"
#include "Strip.h"
#include "Cluster.h"
#include "Constants.h"

namespace utpc {

class Chamber{
 public:
    /** Constructor */
   Chamber(std::string _name) { 
	name = _name; 
        output = new ChamberOutput(); 
        clear();
        if(std::stoi(name.substr(1,1))%2==0)
	      oddNumber=false;

        //if(oddNumber)
	//{
	//	thetaMin=-90.;
        //        thetaMax=0.;
        //}
	//else
	//{
		thetaMin=-90.;
		thetaMax=90.;
	//}
        	
        rMin=0.;
        rMax=100.;

        thetabins=360;
        rbins=500;

        h_hough = new TH2F(Form("h_hough_%s",name.c_str()),Form("h_hough_%s",name.c_str()),thetabins,thetaMin,thetaMax,rbins,rMin,rMax);
   	h_hough->SetDrawOption("colz");
   }
   /** Destructor */
   virtual ~Chamber() { delete output; delete h_hough; clear();};
   /** Copy constructor */
   Chamber(const Chamber &);
   /** Assignment operator */
   Chamber& operator=(const Chamber &);

   //-------------------------------------------------------------------->>
   // Set Functions
   //-------------------------------------------------------------------->>
   //void SetChamberName(std::string _name);
   //-------------------------------------------------------------------->>
   // Member Access
   //-------------------------------------------------------------------->>
   std::string   name;//chamber name i.e. T1
   int				evt;
   bool	  			oddNumber;
   double        		gPosZ;//position along the beam axis in mm
   int           		orientation;//+\- 1 depending on the orientation of the gap with respect to the beam
   double        		rotAngleYZ;//angle with respect to the beam axis in degrees i.e. uTPC angle
   double        		rotAngleXY;//angle of rotation with respect to the nominal x-y plane in degrees
   double        		xOffset;//offset (misalignment) along the precision coordinate in mm
   double        		yOffset;//offset (misalignment) along the second coordinate perp. to the strips in mm
   double        		driftGap;// thickness of the drift gap in mm i.e 5mm
   double        		eAmp;// amplification field in V
   double        		eDrift;// amplification field in V
   double        		vDrift;// drift velocity in mm/us (nominally 4.70459)
   double        		bField;// magnetic field in the chamber in T
   double        		pitch;
   //variables calculated after analysis
   double         		t0;//tzero drift time, this is calculated once at the beginning of the analysis
   //variables for Hough Transform
   int            		thetabins;   
   double         		thetaMin;
   double         		thetaMax;
   int		  		rbins;
   double         		rMin;
   double         		rMax;
   double	  		hough_r;
   double         	 	hough_theta;
   std::vector<Cluster*> 	clusters;
   std::vector<Strip*>   	strips;
    
 
   TH2F *h_hough;
   
   ChamberOutput *output;
   
   bool saveEventDisplay;   

   /** Clear content */
   void clear() {
        evt=0;
        oddNumber=true;
        gPosZ=0.;
        orientation=0.;
        rotAngleYZ=0.;
        rotAngleXY=0.;
        xOffset=0.;
        yOffset=0.;
        driftGap=5;
        eAmp=0.6;
        eDrift=0.3;
        vDrift=4.7;
        bField=0.;
        t0=0.;
        pitch=0.;
   }

   void clear_event() {
        FreeClear(clusters);
        FreeClear(strips);
        h_hough->Reset();
   } 


   /** Print content */
   void print();

   void createHistograms()   {output->createHistograms(name.c_str());};
   void writeHistograms()    {output->writeHistograms();};
   void writeEventDisplay()    {output->writeEventDisplay();};
   
   //void fillClusterHistograms(Cluster *cl);   
   //void fillStripHistograms(Strip *st);
   void fillHistograms();

   void analyse();
   
   void applyHoughFilter();
   void runHoughHisto(double &max_r, double &max_th);
   
   void filterStrips();
   void findt0();
   std::vector<double> findStript0tmax();
   void findClusters();

};
}  // namespace utpc

#endif // CHAMBER_H_INCLUDED
