#if !defined(MYLIB_CONSTANTS_H)
#define MYLIB_CONSTANTS_H 1

//recipe parameters

const bool USE_NEW_ERRORS = true;
const bool USE_CORRECTIONS = true;

//constants for TB 
const double VEL_DRIFT =  0.047;//[mm/ns]
const double GAP_DRIFT = 5.128;//[mm]
const double STRIP_PITCH = 0.4;//[mm]
const double DIFF_TRANS = 0.036;
const double DIFF_LONG = 0.02;

//cut selection
const double CUT_MIN_STRIP_CHARGE = 60.; //min strip charge
const double CUT_MIN_STRIP_SLOPE = 1.;  //min strip FD slope
const double CUT_MIN_STRIP_Y = -2.; //min strip y position
const double CUT_MAX_STRIP_Y = 7.; //max strip y position

const int    CUT_MIN_CLUSTER_SIZE = 3; //min cluster size
const int    CUT_MAX_CLUSTER_SIZE = 15; //max cluster size
const double CUT_MIN_PCHI2 = 0.002;  //min prob(chi2,ndf) for failed fits
const double CUT_MAX_TRACK_SLOPE = 20.;  //max slope of the fitted track

#endif
