//#define MMegasHough_cxx
//#include "MMegasHough.h"
#include <vector>
#include <iostream>
#include <string>
#include <map>
//using namespace std;

#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TH1D.h"
#include <TStyle.h>
#include <TCanvas.h>
#include "TLatex.h"
#include "TROOT.h"
#include "TCanvas.h"
#include "TRandom.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TMarker.h"
#include "TString.h"
#include "TFile.h"
#include "TMath.h"
#include "TLine.h"
#include "TBranch.h"
#include "TTree.h"
#include "TPad.h"
#include "TChain.h"
#include "TBox.h"
#include "TMinuit.h"
#include "TSpectrum.h"
#include "TSystem.h"
#include "Riostream.h"
#include "TROOT.h"
#include "TFitResult.h"




////-----------------------Least Squares----------------------------------------------------------
//void runLeastSquares(std::vector<double> xis, std::vector<double> yis, double slopeHough, double interceptHough, double ResidualLimit,
//                     std::vector<double> &xisAfter, std::vector<double> &yisAfter, double &slopeSquares, double &interceptSquares, std::vector<double> &Residuals){
//   xisAfter.clear();yisAfter.clear();Residuals.clear();
//   double telikoX,telikoY,telikoXSq,telikoYSq,telikoXY;
//   telikoX=telikoY=telikoXSq=telikoYSq=telikoXY=0;
//   int fores = 0;
//   for(int i=0;i<(int)xis.size();i++){
//      double di= (yis[i]-slopeHough*xis[i]-interceptHough)/TMath::Sqrt(slopeHough*slopeHough+1);
//      Residuals.push_back(di);
//      if(TMath::Abs(di)<=ResidualLimit){
//         //*********************** Fit  ********************************************
//         telikoX = telikoX + xis[i];
//         telikoXSq = telikoXSq + (xis[i])*(xis[i]);
//         telikoY = telikoY + yis[i];
//         telikoYSq = telikoYSq + yis[i]*yis[i];
//         telikoXY = telikoXY + (xis[i])*yis[i];
//         fores++;
//         xisAfter.push_back(xis[i]); yisAfter.push_back(yis[i]);
//      }
//   }
//   interceptSquares = (telikoY*telikoXSq-telikoX*telikoXY)/(fores*telikoXSq-(telikoX*telikoX));
//   slopeSquares = (fores*telikoXY-telikoX*telikoY)/(fores*telikoXSq-(telikoX*telikoX));
//}
//
//
////-----------------------Remove Entries with Hough Filter----------------------------------------------------------
//void removeEffectiveEntries(std::vector<double> stripis, std::vector<double> xis, std::vector<double> yis, std::vector<double> yis_err, std::vector<double> amps,
//                           double slopeHough, double interceptHough,double ResidualLowLimit, double ResidualUpLimit,
//                           std::vector<double> &stripisAfter, std::vector<double> &xisAfter, std::vector<double> &yisAfter, std::vector<double> &yis_errAfter, std::vector<double> &ampsAfter,
//                           std::vector<double> &Residuals)
//{
//   int minstrip_xi=10000;
//   stripisAfter.clear();xisAfter.clear();yisAfter.clear(), Residuals.clear();ampsAfter.clear();yis_errAfter.clear();
//   for(int i=0;i<(int)xis.size();i++){
//      double di= (yis[i]-slopeHough*xis[i]-interceptHough)/TMath::Sqrt(slopeHough*slopeHough+1);
//      Residuals.push_back(di);
//      if(di>=ResidualLowLimit && di<=ResidualUpLimit){
//         stripisAfter.push_back(stripis[i]); xisAfter.push_back(xis[i]); yisAfter.push_back(yis[i]); yis_errAfter.push_back(yis_err[i]); ampsAfter.push_back(amps[i]);
//      }
//   }
//   
//}
//
//-----------------------Fit Final Track from Graph----------------------------------------------------------
//void fit_tracklet_graph(TGraphErrors *input_graph, double gap, double &alpha_returned, double &beta_returned, double &returned_angle, double &xhalf, double &chi2_calculated)
//{
//   if(input_graph->GetN()==0) return;
//
//   TF1 f1("f1", "[0]*x + [1]", 20, 80);
//   f1.SetParameter(0, 1);
//   f1.SetParameter(1, 20);
//
//   input_graph->Fit("f1", "Q");
//
//   chi2_calculated = TMath::Prob(f1.GetChisquare(),f1.GetNDF());
//
//   xhalf = f1.GetParameter(0)*gap/2.+f1.GetParameter(1);
//
//   alpha_returned = f1.GetParameter(0);
//   beta_returned  = f1.GetParameter(1);
//   returned_angle = TMath::ATan(alpha_returned)*180/TMath::Pi();
//}
//
//double calc_tracklet_centroid(std::vector <double> x_pos, std::vector<double> amps)
//{
//   double charge_sum=0.0;
//   double pos_charge_sum=0.0;
//   for(size_t iPoint = 0; iPoint<x_pos.size(); ++iPoint)
//   {
//      pos_charge_sum+=x_pos.at(iPoint)*amps.at(iPoint);
//      charge_sum+=amps.at(iPoint);
//   }
//
//   return pos_charge_sum/charge_sum;
//}
//
//double calc_tracklet_comb_point(std::vector <double> tracklet_strips, double utp, double centr)
//{
//   double comb=0.0, xcomb=0.0;
//   double nCut = 4.;double nStrip=tracklet_strips.size();
//   double first_time,last_time;
//   int first_strip=10000;
//   int last_strip=-10000;
//
//   xcomb = (utp*(nStrip/nCut*nStrip/nCut) +centr*(nCut/nStrip*nCut/nStrip))/(nStrip/nCut*nStrip/nCut+nCut/nStrip*nCut/nStrip);
//   
//   if(TMath::Abs(last_strip-first_strip)>4)
//      comb = utp;
//   else if(TMath::Abs(last_strip-first_strip)<3)
//      comb = centr;
//   else comb = (utp+centr)/2;
//
//   //for weigted method
//   comb=xcomb;
//
//   return comb;
//}

template <class C> void FreeClear(C & cntr)
{
    while (!cntr.empty()) {
        typedef typename C::value_type ElementType;
        ElementType el = cntr.front();
        cntr.erase(cntr.begin());
        delete el; el = 0;
    }
}

