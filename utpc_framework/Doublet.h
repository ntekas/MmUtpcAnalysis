#ifndef DOUBLET_H_INCLUDED
#define DOUBLET_H_INCLUDED

#include <vector>
#include <string>
#include <iostream>

#include "TObject.h"
#include "TVector3.h"

#include "Chamber.h"
#include "DoubletOutput.h"
#include "Constants.h"

using namespace std;

namespace utpc {

class Doublet{
 public:
    /** Constructor */
    Doublet(Chamber *ch1, Chamber *ch2) { chamber1 = ch1; chamber2 = ch2; name = ch1->name+ch2->name; clear(); }
    /** Destructor */
    virtual ~Doublet() {};
    /** Copy constructor */
    Doublet(const Doublet &);
    /** Assignment operator */
    Doublet& operator=(const Doublet &);

    //-------------------------------------------------------------------->>
    // Set Functions
    //-------------------------------------------------------------------->>
    //void SetDoubletName(std::string _name);
    //-------------------------------------------------------------------->>
    // Member Access
    //-------------------------------------------------------------------->>
    std::string name;
    DoubletOutput *output = new DoubletOutput();
    Chamber *chamber1;
    Chamber *chamber2;
    
    double posDiff_utpc;
    double posDiff_centroid;
    double posDiff_combo;

    double angleDiff;
    //std::vector <TrigNtup::MdtHit*> Hits();

    /** Clear content */
    void clear() {
    	posDiff_utpc=0.;
	posDiff_centroid=0.;
	posDiff_combo=0.;
	angleDiff=0.;

        //chamber1=nullptr;
        //chamber2=nullptr;
    }
    /** Print content */
    void print();

    void createHistograms()   {output->createHistograms(name.c_str());};
    void writeHistograms()    {output->writeHistograms();};
    void fillHistograms();
    void analyse();
    
};
}  // namespace utpc

#endif // DOUBLET_H_INCLUDED
