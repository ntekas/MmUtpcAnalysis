#ifndef CHAMBEROUTPUT_H_INCLUDED
#define CHAMBEROUTPUT_H_INCLUDED

#include <vector>
#include <string>
#include <iostream>

#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TGraphErrors.h"
#include "Tools.h"
#include "Constants.h"

namespace utpc {

class ChamberOutput{
   public:
   /** Constructor */
   ChamberOutput() {}
   /** Destructor */
   virtual ~ChamberOutput() { /*clear();*/ };
   /** Copy constructor */
   ChamberOutput(const ChamberOutput &);
   /** Assignment operator */
   ChamberOutput& operator=(const ChamberOutput &);

   TH1F *h_cutflow;
   TH1F *h_cutflow_strips;
   TH1F *h_all_times;
   TH1F *h_all_times_filtered;
   TH1F *h_earliest_times;
   TH1F *h_strip_t0s;
   TH1F *h_strip_tmaxs;
   TH2F *h_earliest_times_vs_cluster_width;
   
   TH2F *h_tfit_vs_tfiterror;
   TH2F *h_tfit_vs_qfit;
   TH2F *h_tfiterror_vs_qfit;
   TH1F *h_track_chi2;
   TH1F *h_track_chi2Prob;
   TH2F *h_track_chi2Prob_vs_angle;
   TH1F *h_hough_angle;
   TH1F *h_hough_slope;
   TH1F *h_angle;
   TH1F *h_slope;
   TH2F *h_angle_vs_hough_angle;
   TH2F *h_angle_vs_clsize;
   TH2F *h_angle_vs_clwidth;
   TH2F *h_angle_vs_clcharge;
   TH2F *h_angle_vs_cltime;
   TH1F *h_point_residuals;
   TH2F *h_point_residuals_vs_charge;
   TH1F *h_cluster_charge;
   TH1F *h_cluster_time;
   TH1F *h_cluster_size;
   TH1F *h_cluster_length;
   TH1F *h_cluster_holes;
   TH1F *h_firstLast_correction;   

   

   TCanvas *c_cutflow;
   TCanvas *c_cutflow_strips;

   std::vector <TGraphErrors* > events_graph;
   std::vector <TH2F* > events_hough;
   std::vector <TH1F* > events_h_charge_vs_strip;
   std::vector <TGraph* >  events_trackletGraphPosCentroid;
   std::vector <TGraph* >  events_trackletGraphPosUtpc;
   std::vector <TGraph* >  events_trackletGraphPosComb;

   /** Clear content */
   void clear() {
     delete h_cutflow;
     delete h_cutflow_strips;
     delete h_all_times;
     delete h_all_times_filtered;
     delete h_earliest_times;
     delete h_earliest_times_vs_cluster_width;
     delete h_tfit_vs_tfiterror;
     delete h_tfit_vs_qfit;
     delete h_tfiterror_vs_qfit;
     delete h_track_chi2;
     delete h_track_chi2Prob;
     delete h_track_chi2Prob_vs_angle;
     delete h_hough_angle;
     delete h_hough_slope;
     delete h_angle;
     delete h_slope;
     delete h_angle_vs_hough_angle;
     delete h_angle_vs_clsize;
     delete h_angle_vs_clwidth;
     delete h_angle_vs_clcharge;
     delete h_angle_vs_cltime;
     delete h_point_residuals;
     delete h_point_residuals_vs_charge;
     delete h_cluster_charge;
     delete h_cluster_time;
     delete h_cluster_size;
     delete h_cluster_length;
     delete h_cluster_holes;
     delete h_firstLast_correction;

     delete c_cutflow;
     delete c_cutflow_strips;

     FreeClear(events_graph);
     FreeClear(events_hough);
     FreeClear(events_h_charge_vs_strip);
     FreeClear(events_trackletGraphPosCentroid);
     FreeClear(events_trackletGraphPosUtpc);
     FreeClear(events_trackletGraphPosComb);

   }
   /** Print content */
   void print();

   void createHistograms(const char *chamber);
   void writeHistograms();
   void writeEventDisplay();
};
}  // namespace utpc

#endif // CHAMBEROUTPUT_H_INCLUDED
