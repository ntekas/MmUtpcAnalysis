#ifndef DOUBLETOUTPUT_H_INCLUDED
#define DOUBLETOUTPUT_H_INCLUDED

#include <vector>
#include <string>
#include <iostream>

#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TH1D.h"
#include <TStyle.h>
#include <TCanvas.h>
#include "TLatex.h"
#include "TROOT.h"
#include "TCanvas.h"
#include "TRandom.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TMarker.h"
#include "TString.h"
#include "TFile.h"
#include "TMath.h"
#include "TLine.h"
#include "TBranch.h"
#include "TTree.h"
#include "TPad.h"
#include "TChain.h"
#include "TBox.h"
#include "TMinuit.h"
#include "TSpectrum.h"
#include "TSystem.h"
#include "Riostream.h"
#include "TROOT.h"
#include "TFitResult.h"
#include "TObject.h"
#include "TVector3.h"

#include "AtlasStyle.h"
#include "AtlasLabels.h"
#include "AtlasUtils.h"

#include "Constants.h"

namespace utpc {

class DoubletOutput{
    public:
    /** Constructor */
    DoubletOutput() {}
    /** Destructor */
    virtual ~DoubletOutput() {};
    
    TH1F *h_cutflow;
    TH1F *h_earliest_times_diff;
    TH1F *h_utpc_pos_diff;
    TH1F *h_utpc_pos_diff_sqrt;
    TH1F *h_utpc_pos_diff_cor;
    TH1F *h_utpc_pos_diff_sqrt_cor;
    TH1F *h_utpc_angle_diff;
    TH1F *h_utpc_angle_diff_sqrt;
    TH1F *h_utpc_angle_diff_cor;
    TH1F *h_utpc_angle_diff_sqrt_cor;
    TH1F *h_centroid_pos_diff;
    TH1F *h_centroid_pos_diff_sqrt;
    TH1F *h_centroid_pos_diff_cor;
    TH1F *h_centroid_pos_diff_sqrt_cor;
    TH1F *h_combo_pos_diff;
    TH1F *h_combo_pos_diff_sqrt;
    TH1F *h_combo_pos_diff_cor;
    TH1F *h_combo_pos_diff_sqrt_cor;
   
    TH2F *h_utpc_pos_diff_vs_utpc_pos1;
    TH2F *h_utpc_pos_diff_vs_utpc_pos2;
    TH2F *h_utpc_pos_diff_vs_clsize1;
    TH2F *h_utpc_pos_diff_vs_clsize2;
    TH2F *h_utpc_pos_diff_vs_cllength1;
    TH2F *h_utpc_pos_diff_vs_cllength2;
    TH2F *h_utpc_pos_diff_vs_clholes1;
    TH2F *h_utpc_pos_diff_vs_clholes2;
    TH2F *h_utpc_pos_diff_vs_cltime1;
    TH2F *h_utpc_pos_diff_vs_cltime2;
    TH2F *h_centroid_pos_diff_vs_centroid_pos1;
    TH2F *h_centroid_pos_diff_vs_centroid_pos2;
    TH2F *h_combo_pos_diff_vs_combo_pos1;
    TH2F *h_combo_pos_diff_vs_combo_pos2;
    TH2F *h_utpc_centroid_pos_diff_correl;
    TH2F *h_combo_centroid_pos_diff_correl;
    TH2F *h_combo_utpc_pos_diff_correl;
    
    TCanvas *c_cutflow;
    TCanvas *c_resolution;
    TCanvas *c_resolution_time;
    //TCanvas *c_resolution_combination;
    //TCanvas *c_resolution_centroid;

    /** Clear content */
    void clear() {
    }
    /** Print content */
    void print();

    void createHistograms(const char *doublet);
    void writeHistograms();
    void fitDoubleGauss(TH1F *h_in, TCanvas *c_in, int i_canvas);
};
}  // namespace utpc

#endif // DOUBLETOUTPUT_H_INCLUDED
