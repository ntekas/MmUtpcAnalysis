//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Feb 23 11:36:12 2015 by ROOT version 5.34/10
// from TTree strips_fit/strips_fit
// found on file: run11907_xtalk_processed_fit.root
//////////////////////////////////////////////////////////

#ifndef strips_fit_new_h
#define strips_fit_new_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
#include <vector>
#include <vector>
#include <vector>

// Fixed size dimensions of array or collections stored in the TTree if any.


using namespace std;

class strips_fit_new {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   UInt_t          evt;
   vector<string>  *mm_id;
   vector<unsigned int> *mm_readout;
   vector<unsigned int> *mm_strip;
   vector<double>  *tfit_fd;
   vector<double>  *tfit_fd_slope;
   vector<double>  *tfit_fd_err;
   vector<double>  *tfit_fd_chi2prob;
   vector<short>   *qmax;
   vector<double>  *qmax_fit;

   // List of branches
   TBranch        *b_evt;   //!
   TBranch        *b_mm_id;   //!
   TBranch        *b_mm_readout;   //!
   TBranch        *b_mm_strip;   //!
   TBranch        *b_tfit_fd;   //!
   TBranch        *b_tfit_fd_slope;   //!
   TBranch        *b_tfit_fd_err;   //!
   TBranch        *b_tfit_fd_chi2prob;   //!
   TBranch        *b_qmax;   //!
   TBranch        *b_qmax_fit;   //!

   strips_fit_new(TTree *tree=0);
   virtual ~strips_fit_new();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef strips_fit_new_cxx
strips_fit_new::strips_fit_new(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("run11907_xtalk_processed_fit.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("run11907_xtalk_processed_fit.root");
      }
      f->GetObject("strips_fit",tree);

   }
   Init(tree);
}

strips_fit_new::~strips_fit_new()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t strips_fit_new::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t strips_fit_new::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void strips_fit_new::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   mm_id = 0;
   mm_readout = 0;
   mm_strip = 0;
   tfit_fd = 0;
   tfit_fd_slope = 0;
   tfit_fd_err = 0;
   tfit_fd_chi2prob = 0;
   qmax = 0;
   qmax_fit = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("evt", &evt, &b_evt);
   fChain->SetBranchAddress("mm_id", &mm_id, &b_mm_id);
   fChain->SetBranchAddress("mm_readout", &mm_readout, &b_mm_readout);
   fChain->SetBranchAddress("mm_strip", &mm_strip, &b_mm_strip);
   fChain->SetBranchAddress("tfit_fd", &tfit_fd, &b_tfit_fd);
   fChain->SetBranchAddress("tfit_fd_slope", &tfit_fd_slope, &b_tfit_fd_slope);
   fChain->SetBranchAddress("tfit_fd_err", &tfit_fd_err, &b_tfit_fd_err);
   fChain->SetBranchAddress("tfit_fd_chi2prob", &tfit_fd_chi2prob, &b_tfit_fd_chi2prob);
   fChain->SetBranchAddress("qmax", &qmax, &b_qmax);
   fChain->SetBranchAddress("qmax_fit", &qmax_fit, &b_qmax_fit);
   Notify();
}

Bool_t strips_fit_new::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void strips_fit_new::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t strips_fit_new::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef strips_fit_new_cxx
