//#define MMegasHough_cxx
//#include "MMegasHough.h"
#include <vector>
#include <iostream>
#include <string>
#include <map>
//using namespace std;

#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TH1D.h"
#include <TStyle.h>
#include <TCanvas.h>
#include "TLatex.h"
#include "TROOT.h"
#include "TCanvas.h"
#include "TRandom.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TMarker.h"
#include "TString.h"
#include "TFile.h"
#include "TMath.h"
#include "TLine.h"
#include "TBranch.h"
#include "TTree.h"
#include "TPad.h"
#include "TChain.h"
#include "TBox.h"
#include "TMinuit.h"
#include "TSpectrum.h"
#include "TSystem.h"
#include "Riostream.h"
#include "TROOT.h"
#include "TFitResult.h"

#include "AtlasStyle.h"
#include "AtlasLabels.h"
#include "AtlasUtils.h"

#include "strips_fit_new.h"
#include "Chamber.h"
#include "Doublet.h"
#include "Strip.h"
#include "Constants.h"

//#define VEL_DRIFT 0.047//[mm/ns]
//#define GAP_DRIFT 5.128//[mm]
//#define STRIP_PITCH 0.4//[mm]

//#define DIFF_TRANS 0.036
//#define DIFF_LONG 0.02
