#include "Doublet.h"

using utpc::Doublet;
using namespace std;

/*--------------------------------------------------------------------------------*/
//Copy constructor
/*--------------------------------------------------------------------------------*/
Doublet::Doublet(const Doublet &rhs):
name(rhs.name),
chamber1(rhs.chamber1),
chamber2(rhs.chamber2),
posDiff_utpc(rhs.posDiff_utpc),
posDiff_centroid(rhs.posDiff_centroid),
posDiff_combo(rhs.posDiff_combo),
angleDiff(rhs.angleDiff)
{

}

//---------------------------------------------------------------------------->>
// Assignment operator
//---------------------------------------------------------------------------->>
Doublet& Doublet::Doublet::operator=(const Doublet &rhs) {
    if (this != &rhs) {
      name = rhs.name;
      chamber1 = rhs.chamber1;
      chamber2 = rhs.chamber2;
      posDiff_utpc = rhs.posDiff_utpc;
      posDiff_centroid = rhs.posDiff_centroid;
      posDiff_combo = rhs.posDiff_combo;
      angleDiff = rhs.angleDiff;
    }
    return *this;
}

//---------------------------------------------------------------------------->>
// Print content
//---------------------------------------------------------------------------->>
void Doublet::print() {
    std::cout << "Printing Doublet:" << std::endl;
    std::cout << "\t name = " << name << std::endl;
}

//---------------------------------------------------------------------------->>
// Analyse chamber
//---------------------------------------------------------------------------->>
void Doublet::analyse()
{
    if(chamber1->clusters.size()>0 && chamber2->clusters.size()>0)
    {
	angleDiff = chamber1->clusters.at(0)->trackAngle-chamber2->clusters.at(0)->trackAngle;
    	posDiff_utpc = chamber1->clusters.at(0)->posUtpc-chamber2->clusters.at(0)->posUtpc;
    	posDiff_centroid = chamber1->clusters.at(0)->posCentroid-chamber2->clusters.at(0)->posCentroid;
    	posDiff_combo = chamber1->clusters.at(0)->posCombo-chamber2->clusters.at(0)->posCombo;
    
    	double pChi2_ch1 = TMath::Prob(chamber1->clusters.at(0)->trackChi2,chamber1->clusters.at(0)->trackNdf);
    	double pChi2_ch2 = TMath::Prob(chamber2->clusters.at(0)->trackChi2,chamber2->clusters.at(0)->trackNdf);

    	fillHistograms();
    }
}

void Doublet::fillHistograms()
{
    
   output->h_cutflow->Fill(0);
   if(chamber1->clusters.at(0)->strips.size()>=CUT_MIN_CLUSTER_SIZE && chamber2->clusters.at(0)->strips.size()>=CUT_MIN_CLUSTER_SIZE)
   { 
   	output->h_cutflow->Fill(1);
        if(chamber1->clusters.at(0)->strips.size()<CUT_MAX_CLUSTER_SIZE && chamber2->clusters.at(0)->strips.size()<CUT_MAX_CLUSTER_SIZE)
        {
                output->h_cutflow->Fill(2);
        	if(TMath::Prob(chamber1->clusters.at(0)->trackChi2,chamber1->clusters.at(0)->trackNdf)>CUT_MIN_PCHI2 && TMath::Prob(chamber2->clusters.at(0)->trackChi2,chamber2->clusters.at(0)->trackNdf)>CUT_MIN_PCHI2)
        	{
                        output->h_cutflow->Fill(3);
    			//1-D histograms
    			output->h_utpc_angle_diff->Fill(angleDiff);
    			output->h_utpc_angle_diff_sqrt->Fill(angleDiff/sqrt(2.));
    			output->h_utpc_pos_diff->Fill(posDiff_utpc);
    			output->h_utpc_pos_diff_sqrt->Fill(posDiff_utpc/sqrt(2.));
    			output->h_centroid_pos_diff->Fill(posDiff_centroid);
    			output->h_centroid_pos_diff_sqrt->Fill(posDiff_centroid/sqrt(2.));
    			output->h_combo_pos_diff->Fill(posDiff_combo);
    			output->h_combo_pos_diff_sqrt->Fill(posDiff_combo/sqrt(2.));

            output->h_earliest_times_diff->Fill((chamber1->clusters.at(0)->earliest_time-chamber2->clusters.at(0)->earliest_time)/sqrt(2.));

    			//2-D histograms
    			output->h_utpc_pos_diff_vs_utpc_pos1->Fill(chamber1->clusters.at(0)->posUtpc,posDiff_utpc);
    			output->h_utpc_pos_diff_vs_utpc_pos2->Fill(chamber2->clusters.at(0)->posUtpc,posDiff_utpc);
    			output->h_centroid_pos_diff_vs_centroid_pos1->Fill(chamber1->clusters.at(0)->posCentroid,posDiff_centroid);
    			output->h_centroid_pos_diff_vs_centroid_pos2->Fill(chamber2->clusters.at(0)->posCentroid,posDiff_centroid);
    			output->h_combo_pos_diff_vs_combo_pos1->Fill(chamber1->clusters.at(0)->posCombo,posDiff_combo);
    			output->h_combo_pos_diff_vs_combo_pos2->Fill(chamber2->clusters.at(0)->posCombo,posDiff_combo);
    			output->h_utpc_centroid_pos_diff_correl->Fill(posDiff_centroid,posDiff_utpc);
    			output->h_combo_centroid_pos_diff_correl->Fill(posDiff_combo,posDiff_centroid);
    			output->h_combo_utpc_pos_diff_correl->Fill(posDiff_combo,posDiff_utpc);
		}
	}
   }
}
