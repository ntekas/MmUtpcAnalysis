#include "Chamber.h"

using utpc::Chamber;
using utpc::ChamberOutput;
using utpc::Cluster;
using namespace std;

/*--------------------------------------------------------------------------------*/
//Copy constructor
/*--------------------------------------------------------------------------------*/
Chamber::Chamber(const Chamber &rhs):
name(rhs.name),
evt(rhs.evt),
oddNumber(rhs.oddNumber),
gPosZ(rhs.gPosZ),
orientation(rhs.orientation),
rotAngleYZ(rhs.rotAngleYZ),
rotAngleXY(rhs.rotAngleXY),
xOffset(rhs.xOffset),
yOffset(rhs.yOffset),
driftGap(rhs.driftGap),
eAmp(rhs.eAmp),
eDrift(rhs.eDrift),
vDrift(rhs.vDrift),
bField(rhs.bField),
t0(rhs.t0),
pitch(rhs.pitch),
thetabins(rhs.thetabins),
thetaMin(rhs.thetaMin),
thetaMax(rhs.thetaMax),
rbins(rhs.rbins),
rMin(rhs.rMin),
rMax(rhs.rMax),
hough_r(rhs.hough_r),
hough_theta(rhs.hough_theta),
clusters(rhs.clusters),
strips(rhs.strips),
h_hough(rhs.h_hough),
output(rhs.output)
{

}

//---------------------------------------------------------------------------->>
// Assignment operator
//---------------------------------------------------------------------------->>
Chamber& Chamber::Chamber::operator=(const Chamber &rhs) {
    if (this != &rhs) {
      name = rhs.name;
      evt = rhs.evt;
      oddNumber = rhs.oddNumber;
      gPosZ = rhs.gPosZ;
      orientation = rhs.orientation;
      rotAngleYZ = rhs.rotAngleYZ;
      rotAngleXY = rhs.rotAngleXY;
      xOffset = rhs.xOffset;
      yOffset = rhs.yOffset;
      driftGap = rhs.driftGap;
      eAmp = rhs.eAmp;
      eDrift = rhs.eDrift;
      vDrift = rhs.vDrift;
      bField = rhs.bField;
      t0 = rhs.t0;
      thetabins = rhs.thetabins;
      thetaMin = rhs.thetaMin;
      thetaMax = rhs.thetaMax;
      rbins = rhs.rbins;
      rMin = rhs.rMin;
      rMax = rhs.rMax;
      hough_r = rhs.hough_r;
      hough_theta = rhs.hough_theta;
      pitch = rhs.pitch;
      clusters = rhs.clusters;
      strips = rhs.strips;
      h_hough = rhs.h_hough;
      output = rhs.output;
    }
    return *this;
}

//---------------------------------------------------------------------------->>
// Print Roi content
//---------------------------------------------------------------------------->>
void Chamber::print() {
    std::cout << "Printing Chamber:" << std::endl;
    std::cout << "\t name = " << name << std::endl;
}


void Chamber::analyse()
{
    //print();
    filterStrips();
    findClusters();
    fillHistograms();
}

void Chamber::fillHistograms()
{

    for(auto it : clusters)
    {
	   it->fillHistos(output);
    }

}

void Chamber::applyHoughFilter()
{
   double max_r=0.;
   double max_th=0.;
   double cut = 1.0;

   runHoughHisto(max_r,max_th);

   hough_r = max_r;
   hough_theta = max_th;
 
   double slope_hough = TMath::Tan((max_th-90)*TMath::DegToRad());
   double intercept_hough = max_r/TMath::Cos((max_th-90)*TMath::DegToRad());
   
   int minstrip_xi=10000;
   for(auto it = strips.begin(); it != strips.end();)
   {
   	double di= ((*it)->posY-slope_hough*(*it)->posX-intercept_hough)/TMath::Sqrt(slope_hough*slope_hough+1);
        if(abs(di)>cut)
        {
           it = strips.erase(it);
        }
        else
        {
           ++it;
        }
   }
}


void Chamber::runHoughHisto(double &max_r, double &max_th){
   

   int bin_theta=0;
   int bin_r=0;
   int bin=0;

   int max_lat=-10000;
   int map_maxbin1th = 0;
   int map_maxbin1r = 0;
   int map_maxbin1 = 0;
   
   int n_neigbours=1;
   
   int thetabins = h_hough->GetXaxis()->GetNbins();
   int rbins = h_hough->GetYaxis()->GetNbins();

   float theta1 = h_hough->GetXaxis()->GetBinLowEdge(h_hough->GetXaxis()->GetFirst());
   float theta2 = h_hough->GetXaxis()->GetBinLowEdge(h_hough->GetXaxis()->GetLast());
   float r1 = h_hough->GetYaxis()->GetBinLowEdge(h_hough->GetYaxis()->GetFirst());
   float r2 = h_hough->GetYaxis()->GetBinLowEdge(h_hough->GetYaxis()->GetLast());

   //std::cout<<"theta1 "<<theta1<<" theta2 "<<theta2<<" r1 "<<r1<<" r2 "<<r2<<std::endl;
 
   for(auto it : strips){
      for(float theta=theta1;theta<=theta2;theta+=(float)(theta2-theta1)/thetabins){
         
         double middleOfThetaBins=theta+((theta2-theta1)/thetabins)/2.;  // middle of a bin
         double rTeliko = it->posX*TMath::Cos(TMath::DegToRad()*middleOfThetaBins) + it->posY*TMath::Sin(TMath::DegToRad()*middleOfThetaBins);
         
         //std::cout<<"theta "<<middleOfThetaBins<<" x "<<it->posX<<" y "<<it->posY<<" r "<<rTeliko<<std::endl;

         if((rTeliko>r1 && rTeliko<r2) && (middleOfThetaBins>theta1 && middleOfThetaBins<theta2)) {
            
            bin_theta = h_hough->GetXaxis()->FindBin(middleOfThetaBins);
            bin_r = h_hough->GetYaxis()->FindBin(rTeliko);
            bin = h_hough->GetBin(bin_theta, bin_r,0);
            //std::cout<<"bin_theta "<<bin_theta<<" bin_r "<<bin_r<<std::endl;
            //std::cout<<"Fill legendre "<<" theta "<<middleOfThetaBins<<" r "<<rTeliko<<std::endl;

            //h_hough->Fill(middleOfThetaBins,rTeliko);
            h_hough->SetBinContent(h_hough->GetBin(bin_theta, bin_r,0),h_hough->GetBinContent(bin_theta, bin_r,0)+1);

            if(max_lat<h_hough->GetBinContent(bin_theta,bin_r,0))
            {
               max_lat = h_hough->GetBinContent(bin_theta,bin_r,0);
               map_maxbin1th = bin_theta;
               map_maxbin1r = bin_r;
               map_maxbin1 = bin;
               //std::cout<<"New max bin with entries "<<max_lat<<" bin theta "<<map_maxbin1th<<" bin r "<<bin_r<<std::endl;
            }

            for(int i=1; i<=n_neigbours; i++)
            {
               h_hough->SetBinContent(h_hough->GetBin(bin_theta, bin_r+i,0),
                                      h_hough->GetBinContent(bin_theta, bin_r+i,0)+1);
            if(max_lat<h_hough->GetBinContent(bin_theta,bin_r+i))
            {
               max_lat = h_hough->GetBinContent(bin_theta,bin_r+i);
               map_maxbin1th = bin_theta;
               map_maxbin1r = bin_r+i;
               map_maxbin1 = bin;
               //std::cout<<"New max bin "<<" bin theta "<<map_maxbin1th<<" bin r "<<map_maxbin1r<<std::endl;
            }

               h_hough->SetBinContent(h_hough->GetBin(bin_theta, bin_r-i,0),
                                      h_hough->GetBinContent(bin_theta, bin_r-i,0)+1);
            if(max_lat<h_hough->GetBinContent(bin_theta,bin_r-i))
            {
               max_lat = h_hough->GetBinContent(bin_theta,bin_r-i);
               map_maxbin1th = bin_theta;
               map_maxbin1r = bin_r-i;
               map_maxbin1 = bin;
               //std::cout<<"New max bin "<<" bin theta "<<map_maxbin1th<<" bin r "<<map_maxbin1r<<std::endl;
            }
               h_hough->SetBinContent(h_hough->GetBin(bin_theta+i, bin_r,0),
                                      h_hough->GetBinContent(bin_theta+i, bin_r,0)+1);
            if(max_lat<h_hough->GetBinContent(bin_theta+i,bin_r))
            {
               max_lat = h_hough->GetBinContent(bin_theta+i,bin_r);
               map_maxbin1th = bin_theta+i;
               map_maxbin1r = bin_r;
               map_maxbin1 = bin;
               //std::cout<<"New max bin "<<" bin theta "<<map_maxbin1th<<" bin r "<<map_maxbin1r<<std::endl;
            }
               h_hough->SetBinContent(h_hough->GetBin(bin_theta-i, bin_r,0),
                                      h_hough->GetBinContent(bin_theta-i, bin_r,0)+1);
            if(max_lat<h_hough->GetBinContent(bin_theta-i,bin_r))
            {
               max_lat = h_hough->GetBinContent(bin_theta-i,bin_r);
               map_maxbin1th = bin_theta-i;
               map_maxbin1r = bin_r;
               map_maxbin1 = bin;
               //std::cout<<"New max bin "<<" bin theta "<<map_maxbin1th<<" bin r "<<map_maxbin1r<<std::endl;
            }
            }
            

         }
      }
   }
   
   max_r = h_hough->GetYaxis()->GetBinCenter(map_maxbin1r);
   max_th = h_hough->GetXaxis()->GetBinCenter(map_maxbin1th);
}

///< Filter strips to remove low signals or bad FD fits
void Chamber::filterStrips()
{

    unsigned i=0;
    while(i<strips.size())
    {
        output->h_cutflow_strips->Fill(0);
	if(strips.at(i)->charge>CUT_MIN_STRIP_CHARGE)
	{
		output->h_cutflow_strips->Fill(1);
		if(strips.at(i)->fdSlope>CUT_MIN_STRIP_SLOPE)
		{
			output->h_cutflow_strips->Fill(2);
			if(strips.at(i)->posY>CUT_MIN_STRIP_Y)
			{
				output->h_cutflow_strips->Fill(3);
				if(strips.at(i)->posY<CUT_MAX_STRIP_Y)
				{
					output->h_cutflow_strips->Fill(4);
					++i;
				}
				else
        			{
					strips.erase(strips.begin()+i);	
				}      	
			}
			else
        		{
				strips.erase(strips.begin()+i);	
			}      	
		}
		else
        	{
			strips.erase(strips.begin()+i);	
		}      	
        }
	else
        {
		strips.erase(strips.begin()+i);	
	}      	
    }
}
///< Loop over all strips' times to determine the t0 of the chamber
void Chamber::findt0()
{
    if(output->h_all_times->GetEntries()==0)
	return;
    ///< function to fit the inclusive time distribution
    TF1 f_t0{"f_t0", "[0] / ((1 + TMath::Exp(([1]-x)/[2]))) + [6]/(1+TMath::Exp((x-[3])/[4])) + [5]", 0, 400};
    ///< parameters to be made generic 
    f_t0.SetParameters(10000, 99, 90, 300, 7, -100, 10000);
    f_t0.SetParLimits(0, 1, 100000);
    f_t0.SetParLimits(1, 10, 100000);
    f_t0.SetParLimits(2, 0, 100000);
    f_t0.SetParLimits(3, 0, 100000);
    f_t0.SetParLimits(5, -500000, 500000);
    f_t0.SetParLimits(4, 1, 100000);
    f_t0.SetParLimits(6, 0, 100000);    
    f_t0.SetParameters(100, 99, 90, 300, 7, -100, 52);
    
    output->h_all_times->Fit("f_t0","QR");output->h_all_times->Fit("f_t0","QR");output->h_all_times->Fit("f_t0","QR");

    t0 = f_t0.GetParameter(1);
}

std::vector<double> Chamber::findStript0tmax()
{
    std::vector<double> t0s;
    t0s.clear();
   
    if(output->h_all_times->GetEntries()==0)
      return t0s;
    output->h_all_times->Scale(1/output->h_all_times->GetEntries());
    ///< function to fit the inclusive time distribution
    TF1 f_t0{"f_t0", "[0] / ((1 + TMath::Exp(([1]-x)/[2]))) + [6]/(1+TMath::Exp((x-[3])/[4])) + [5]", 0, 400};
    ///< parameters to be made generic
    f_t0.SetParameters(0.01, 99, 90, 300, 7, -0.01, 0.01);
    f_t0.SetParLimits(0, 0., 0.1);
    f_t0.SetParLimits(1, 10, 100000);
    f_t0.SetParLimits(2, 0, 100000);
    f_t0.SetParLimits(3, 0, 100000);
    f_t0.SetParLimits(5, -0.03, 0.03);
    f_t0.SetParLimits(4, 1, 100000);
    f_t0.SetParLimits(6, 0., 0.1);

    //output->h_all_times->Fit("f_t0","QR");output->h_all_times->Fit("f_t0","QR");
   
    TFitResultPtr r= output->h_all_times->Fit("f_t0","QR");
    Int_t fitStatus = r;
    //std::cout<<fitStatus<<std::endl;
    if(fitStatus==0){
      t0s.push_back(f_t0.GetParameter(1));
      t0s.push_back(f_t0.GetParameter(3));
    }
    output->h_all_times->Reset();
   
    return t0s;
}


void Chamber::findClusters()
{
    h_hough->Reset();
    applyHoughFilter();

    Cluster *cluster_tmp =  new Cluster();
    cluster_tmp->houghAngle = hough_theta-90.;
    //cluster_tmp->houghAngle = -hough_theta;
    cluster_tmp->houghSlope = TMath::Tan((cluster_tmp->houghAngle)*TMath::DegToRad());
    cluster_tmp->houghIntercept = hough_r/TMath::Cos((cluster_tmp->houghAngle)*TMath::DegToRad());
    cluster_tmp->oddChamber = oddNumber;
    
    for(auto it : strips)
    {
    	cluster_tmp->strips.push_back(it);
    }

    cluster_tmp->sortStrips();

    if(USE_CORRECTIONS && cluster_tmp->strips.size()>=2)
    {
    	cluster_tmp->applyCorrections();
    }

    cluster_tmp->calculateCluster();
    clusters.push_back(cluster_tmp);
	
    cluster_tmp->h_charge_vs_strip->SetName(Form("h_charge_vs_strips_%s_evt%i",name.c_str(),evt));
    cluster_tmp->h_charge_vs_strip->SetTitle(Form("h_charge_vs_strips_%s_evt%i",name.c_str(),evt));
    
    if(output->events_hough.size()<=100)
    {
    	TH2F *h_hough_write = new TH2F(*h_hough);    

    	h_hough_write->SetName(Form("h_hough_%s_evt%i",name.c_str(),evt));
    	h_hough_write->SetTitle(Form("h_hough_%s_evt%i",name.c_str(),evt));
    	h_hough_write->SetDrawOption("colz");  

    	TGraphErrors *trackletGraph_write = new TGraphErrors(*cluster_tmp->trackletGraph);
    	trackletGraph_write->GetHistogram()->SetName(Form("g_track_%s_evt%i",name.c_str(),evt));
    	trackletGraph_write->SetName(Form("g_track_%s_evt%i",name.c_str(),evt));
    	trackletGraph_write->SetTitle(Form("g_track_%s_evt%i",name.c_str(),evt));

        TH1F *h_charge_vs_strips_write = new TH1F(*cluster_tmp->h_charge_vs_strip);
    	h_charge_vs_strips_write->SetName(Form("h_charge_vs_strips_%s_evt%i",name.c_str(),evt));
    	h_charge_vs_strips_write->SetTitle(Form("h_charge_vs_strips_%s_evt%i",name.c_str(),evt));
    	
        output->events_hough.push_back(h_hough_write);
    	output->events_graph.push_back(trackletGraph_write);
        output->events_h_charge_vs_strip.push_back(h_charge_vs_strips_write);
        cluster_tmp->h_hough = h_hough_write;
    }
    else
    {
	cluster_tmp->h_hough = h_hough;
    }
}
