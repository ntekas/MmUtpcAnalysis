#ifndef CLUSTER_H_INCLUDED
#define CLUSTER_H_INCLUDED

#include <vector>
#include <string>
#include <iostream>


#include "TObject.h"
#include "TVector3.h"
#include "TGraphErrors.h"
#include "TH2.h"
#include "TF1.h"

#include "Strip.h"
#include "ChamberOutput.h"
#include "Constants.h"

namespace utpc {

class Cluster{
 public:
    /** Constructor */
    Cluster() {clear();}
    /** Destructor */
    virtual ~Cluster() {if(trackletGraph!=nullptr){delete trackletGraph;trackletGraph=nullptr;delete h_charge_vs_strip; h_charge_vs_strip=nullptr;}};
    /** Copy constructor */
    Cluster(const Cluster &);
    /** Assignment operator */
    Cluster& operator=(const Cluster &);

    //-------------------------------------------------------------------->>
    // Set Functions
    //-------------------------------------------------------------------->>
    //-------------------------------------------------------------------->>
    // Member Access
    //-------------------------------------------------------------------->>
    int                     type;//1 is from Hough, 2 is topological
    bool		    oddChamber;
    double                  posCentroid;//cluster position using the centroid method
    double                  posUtpc;//cluster position using the utpc method
    double                  posCombo;//cluster position using the combination
    double                  posCentroid_cor;//cluster position using the centroid method
    double                  posUtpc_cor;//cluster position using the utpc method
    double                  posCombo_cor;//cluster position using the combination
    double                  houghAngle;//hough angle in degrees
    double                  houghSlope;//hough slope
    double                  houghIntercept;//houghc intercept
    int			             houghMaxBinEntries;
    double                  trackAngle;//uptc angle in degrees
    double                  trackSlope;//utpc slope
    double                  trackIntercept;//utpc intercept
    double                  trackChi2;//utpc chi2
    double                  trackNdf;//utpc ndf
    double                  trackAngle_cor;//uptc angle in degrees
    double                  trackSlope_cor;//utpc slope
    double                  trackIntercept_cor;//utpc intercept
    double                  trackChi2_cor;//utpc chi2
    double                  trackNdf_cor;//utpc ndf
    double                  charge;//cluster charge
    double                  time;//cluster time
    double                  earliest_time;//earliest time
    double                  timeSpan;//max time - min time in cluster
    int                     size;//number of strips in cluster
    int                     length;//max-min strip in cluster
    int                     nHoles;//number of empty strips within cluster
    std::vector<Strip *>    strips;//cluster strips
    double 		    corFirst;
    double 		    corLast;
    
    TGraphErrors 	    *trackletGraph;
    TH2F 		    *h_hough;
    TH1F 		    *h_charge_vs_strip;
    TGraph		    *trackletGraphPosCentroid;
    TGraph		    *trackletGraphPosUtpc;
    TGraph		    *trackletGraphPosComb;


    bool 		    saveEventDisplay;

    /** Clear content */
    void clear() {
        oddChamber=true;
        type=0.;
        posCentroid=0.;
        posUtpc=0.;
        posCombo=0.;
        posCentroid_cor=0.;
        posUtpc_cor=0.;
        posCombo_cor=0.;
        houghAngle=0.;
        houghSlope=0.;
        houghIntercept=0.;
        houghMaxBinEntries=0;
        trackAngle=0.;
        trackSlope=0.;
        trackIntercept=0.;
        trackChi2=0.;
        trackNdf=0.;
        trackAngle_cor=0.;
        trackSlope_cor=0.;
        trackIntercept_cor=0.;
        trackChi2_cor=0.;
        trackNdf_cor=0.;
        charge=0.;
        time=0.;
        earliest_time=0.;
        size=0;
        length=0;
        nHoles=0;
        corFirst=0.;
        corLast=0.;
    }
    /** Print content */
    void print();
    void fillHistos(ChamberOutput *output);
    void transformRecoAngles();
    void applyCorrections();
    void sortStrips();    
    void correctFirstLastStrips();
    void filterFirstLastStrips();
    
    void calculateCluster();
    void runUtpc();
    void runCentroid();
    void runCombination();
};
}  // namespace utpc

#endif // CLUSTER_H_INCLUDED
