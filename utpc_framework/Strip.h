#ifndef STRIP_H_INCLUDED
#define STRIP_H_INCLUDED


#include <math.h>
#include <iostream>

#include "Constants.h"

namespace utpc {

class Strip{
 public:
    /** Constructor */
    Strip() {clear();}
    /** Destructor */
    virtual ~Strip() {};
    /** Copy constructor */
    Strip(const Strip &);
    /** Assignment operator */
    Strip& operator=(const Strip &);

    //-------------------------------------------------------------------->>
    // Set Functions
    //-------------------------------------------------------------------->>
    //-------------------------------------------------------------------->>
    // Member Access
    //-------------------------------------------------------------------->>
    int           number;//strip number within one chamber
    double        posX;//strip number converted in distanec (mm)
    double        posY;//position along drift gap (mm)
    double        charge;//charge of the strip (qmax)
    double        time;//time of the strip (time from FD fit)
    double        timeErr;//error of the time measurement (from FD fit)
    double        fdSlope;//slope of the FD fit
    double	      pitch;

    double	      errX;
    double	      errY;
    double        t0;

    void assignErrors(int clSize, double clCharge, double vDrift, double diffTrans, double diffLong, bool isEdge);

    /** Clear content */
    void clear() {
         number=0.;
         posX=0.;
         posY=0.;
         charge=0.;
         time=0.;
	      timeErr=0.;
 	      fdSlope=0.;
	      pitch=0.;

	      errX=0.;
	      errY=0.;
         t0=0;
    }
    /** Print content */
    void print();

};
}  // namespace utpc

#endif // STRIP_H_INCLUDED
