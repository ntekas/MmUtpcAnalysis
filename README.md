Scripts for uTPC analysis of 2012 MM Test Beam data

The data files can be found in my public folder in lxplus along with information about the HV and the chambers' angle

/afs/cern.ch/user/n/ntekas/work/public/Data


The processing is done in the following order

->Fist run the function write_fit_ntuple() of the process_fit_APC.C script.
This processes the raw files, fitting the APV25 integrated charge per channel with the Fermi-Dirac function and outputs a new ntuple "runXXXX_fit.root" 
The processed files have allready been produced and exist on afs

->Run the process function of the uTpc_analysis.C script

process(const char *  file, int angle, bool debug, bool write_file)

where the input file should be the processed one. The angle is the true angle of the chambers. Debug should be sent to true for debugging single events while write_file should be set to false.