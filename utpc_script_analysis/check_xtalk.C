#include <vector>
#pragma link C++ class vector<vector<short> >+;
#include <iostream>
#include <string>
using namespace std;
#include "Riostream.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TH1D.h"
#include "TROOT.h"
#include "TCanvas.h"
#include "TRandom.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TFile.h"
#include "TMath.h"
#include "TLatex.h"
#include "TBranch.h"
#include "TTree.h"
#include "TKey.h"
#include "TClass.h"
#include "TDirectory.h"

TFile *root_file;

#include "apv_raw.C"
#include "APV_RAW_PED.C"
apv_raw *raw;
APV_RAW_PED *apv_raw_ped;

void run(const char *  file){

	TFile *root_file = new TFile(file,"READ");
	
	raw = new apv_raw((TTree*)root_file->Get("raw"));

	int entries = (raw->fChain)->GetEntries();

  TH2F *h 			 		= new TH2F("h", "h", 2000, 0, 2000, 2000, 0, 2000);
  TH2F *h_channel_0 = new TH2F("h_chip_0", "h_chip_0", 130, 0, 130, 130, 0, 130);
  TH2F *h_channel_1 = new TH2F("h_chip_1", "h_chip_1", 130, 0, 130, 130, 0, 130);
  TH2F *h_channel_2 = new TH2F("h_chip_2", "h_chip_2", 130, 0, 130, 130, 0, 130);
  TH2F *h_channel_3 = new TH2F("h_chip_3", "h_chip_3", 130, 0, 130, 130, 0, 130);
  TH2F *h_channel_4 = new TH2F("h_chip_4", "h_chip_4", 130, 0, 130, 130, 0, 130);
  TH2F *h_channel_5 = new TH2F("h_chip_5", "h_chip_5", 130, 0, 130, 130, 0, 130);
  TH2F *h_channel_6 = new TH2F("h_chip_6", "h_chip_6", 130, 0, 130, 130, 0, 130);
  TH2F *h_channel_7 = new TH2F("h_chip_7", "h_chip_7", 130, 0, 130, 130, 0, 130);
  TH2F *h_channel_8 = new TH2F("h_chip_8", "h_chip_8", 130, 0, 130, 130, 0, 130);
  TH2F *h_channel_9 = new TH2F("h_chip_9", "h_chip_9", 130, 0, 130, 130, 0, 130);
  TH2F *h_channel_10 = new TH2F("h_chip_10", "h_chip_10", 130, 0, 130, 130, 0, 130);
  TH2F *h_channel_11 = new TH2F("h_chip_11", "h_chip_11", 130, 0, 130, 130, 0, 130);
  TH2F *h_channel_12 = new TH2F("h_chip_12", "h_chip_12", 130, 0, 130, 130, 0, 130);
  TH2F *h_channel_13 = new TH2F("h_chip_13", "h_chip_13", 130, 0, 130, 130, 0, 130);
  TH2F *h_channel_14 = new TH2F("h_chip_14", "h_chip_14", 130, 0, 130, 130, 0, 130);
  TH2F *h_channel_15 = new TH2F("h_chip_15", "h_chip_15", 130, 0, 130, 130, 0, 130);
   
  TH2F *h_channel_all = new TH2F("h_channel_all", "h_channel_all", 130, 0, 130, 130, 0, 130);

//  entries = 10000;

	for(int iEntry=0; iEntry<entries; iEntry++) {

		if(iEntry%200==0)
      cout << "\r" << (double)iEntry/(double)entries*100 << "\% processed" << flush;

		(raw->fChain)->GetEntry(iEntry);
		double which_channel = -100;

		// srsChip 1
		double temp = 0.;

      for (int j=0; j<(int)raw->apv_fecNo->size(); j++) {
         if (raw->apv_id->at(j)==0) {
            for (int i=0; i<(int)raw->apv_q->at(j).size(); i++) {
               if (raw->apv_q->at(j).at(i) > temp) {
                  temp = raw->apv_q->at(j).at(i);
                  which_channel = raw->apv_ch->at(j);
               }
            }
         }
      }
      for (int j=0; j<(int)raw->apv_fecNo->size(); j++)
         if (raw->apv_id->at(j)==0)
            h_channel_0->Fill(which_channel, raw->apv_ch->at(j));
      
      
      for (int j=0; j<(int)raw->apv_fecNo->size(); j++) {
    	if (raw->apv_id->at(j)==1) {
				for (int i=0; i<(int)raw->apv_q->at(j).size(); i++) {
					if (raw->apv_q->at(j).at(i) > temp) {
						temp = raw->apv_q->at(j).at(i);
						which_channel = raw->apv_ch->at(j);
					}
				}
			}
		}
   	for (int j=0; j<(int)raw->apv_fecNo->size(); j++)
    	if (raw->apv_id->at(j)==1)
				h_channel_1->Fill(which_channel, raw->apv_ch->at(j));

      for (int j=0; j<(int)raw->apv_fecNo->size(); j++) {
         if (raw->apv_id->at(j)==2) {
            for (int i=0; i<(int)raw->apv_q->at(j).size(); i++) {
               if (raw->apv_q->at(j).at(i) > temp) {
                  temp = raw->apv_q->at(j).at(i);
                  which_channel = raw->apv_ch->at(j);
               }
            }
         }
      }
      for (int j=0; j<(int)raw->apv_fecNo->size(); j++)
         if (raw->apv_id->at(j)==2)
            h_channel_2->Fill(which_channel, raw->apv_ch->at(j));

      for (int j=0; j<(int)raw->apv_fecNo->size(); j++) {
         if (raw->apv_id->at(j)==3) {
            for (int i=0; i<(int)raw->apv_q->at(j).size(); i++) {
               if (raw->apv_q->at(j).at(i) > temp) {
                  temp = raw->apv_q->at(j).at(i);
                  which_channel = raw->apv_ch->at(j);
               }
            }
         }
      }
      for (int j=0; j<(int)raw->apv_fecNo->size(); j++)
         if (raw->apv_id->at(j)==3)
            h_channel_3->Fill(which_channel, raw->apv_ch->at(j));

      for (int j=0; j<(int)raw->apv_fecNo->size(); j++) {
         if (raw->apv_id->at(j)==4) {
            for (int i=0; i<(int)raw->apv_q->at(j).size(); i++) {
               if (raw->apv_q->at(j).at(i) > temp) {
                  temp = raw->apv_q->at(j).at(i);
                  which_channel = raw->apv_ch->at(j);
               }
            }
         }
      }
      for (int j=0; j<(int)raw->apv_fecNo->size(); j++)
         if (raw->apv_id->at(j)==4)
            h_channel_4->Fill(which_channel, raw->apv_ch->at(j));

      for (int j=0; j<(int)raw->apv_fecNo->size(); j++) {
         if (raw->apv_id->at(j)==5) {
            for (int i=0; i<(int)raw->apv_q->at(j).size(); i++) {
               if (raw->apv_q->at(j).at(i) > temp) {
                  temp = raw->apv_q->at(j).at(i);
                  which_channel = raw->apv_ch->at(j);
               }
            }
         }
      }
      for (int j=0; j<(int)raw->apv_fecNo->size(); j++)
         if (raw->apv_id->at(j)==5)
            h_channel_5->Fill(which_channel, raw->apv_ch->at(j));

      for (int j=0; j<(int)raw->apv_fecNo->size(); j++) {
         if (raw->apv_id->at(j)==6) {
            for (int i=0; i<(int)raw->apv_q->at(j).size(); i++) {
               if (raw->apv_q->at(j).at(i) > temp) {
                  temp = raw->apv_q->at(j).at(i);
                  which_channel = raw->apv_ch->at(j);
               }
            }
         }
      }
      for (int j=0; j<(int)raw->apv_fecNo->size(); j++)
         if (raw->apv_id->at(j)==6)
            h_channel_6->Fill(which_channel, raw->apv_ch->at(j));

      for (int j=0; j<(int)raw->apv_fecNo->size(); j++) {
         if (raw->apv_id->at(j)==7) {
            for (int i=0; i<(int)raw->apv_q->at(j).size(); i++) {
               if (raw->apv_q->at(j).at(i) > temp) {
                  temp = raw->apv_q->at(j).at(i);
                  which_channel = raw->apv_ch->at(j);
               }
            }
         }
      }
      for (int j=0; j<(int)raw->apv_fecNo->size(); j++)
         if (raw->apv_id->at(j)==7)
            h_channel_7->Fill(which_channel, raw->apv_ch->at(j));

      for (int j=0; j<(int)raw->apv_fecNo->size(); j++) {
         if (raw->apv_id->at(j)==8) {
            for (int i=0; i<(int)raw->apv_q->at(j).size(); i++) {
               if (raw->apv_q->at(j).at(i) > temp) {
                  temp = raw->apv_q->at(j).at(i);
                  which_channel = raw->apv_ch->at(j);
               }
            }
         }
      }
      for (int j=0; j<(int)raw->apv_fecNo->size(); j++)
         if (raw->apv_id->at(j)==8)
            h_channel_8->Fill(which_channel, raw->apv_ch->at(j));

      for (int j=0; j<(int)raw->apv_fecNo->size(); j++) {
         if (raw->apv_id->at(j)==9) {
            for (int i=0; i<(int)raw->apv_q->at(j).size(); i++) {
               if (raw->apv_q->at(j).at(i) > temp) {
                  temp = raw->apv_q->at(j).at(i);
                  which_channel = raw->apv_ch->at(j);
               }
            }
         }
      }
      for (int j=0; j<(int)raw->apv_fecNo->size(); j++)
         if (raw->apv_id->at(j)==9)
            h_channel_9->Fill(which_channel, raw->apv_ch->at(j));

      for (int j=0; j<(int)raw->apv_fecNo->size(); j++) {
         if (raw->apv_id->at(j)==10) {
            for (int i=0; i<(int)raw->apv_q->at(j).size(); i++) {
               if (raw->apv_q->at(j).at(i) > temp) {
                  temp = raw->apv_q->at(j).at(i);
                  which_channel = raw->apv_ch->at(j);
               }
            }
         }
      }
      for (int j=0; j<(int)raw->apv_fecNo->size(); j++)
         if (raw->apv_id->at(j)==10)
            h_channel_10->Fill(which_channel, raw->apv_ch->at(j));
      
      for (int j=0; j<(int)raw->apv_fecNo->size(); j++) {
         if (raw->apv_id->at(j)==11) {
            for (int i=0; i<(int)raw->apv_q->at(j).size(); i++) {
               if (raw->apv_q->at(j).at(i) > temp) {
                  temp = raw->apv_q->at(j).at(i);
                  which_channel = raw->apv_ch->at(j);
               }
            }
         }
      }
      for (int j=0; j<(int)raw->apv_fecNo->size(); j++)
         if (raw->apv_id->at(j)==11)
            h_channel_11->Fill(which_channel, raw->apv_ch->at(j));

      for (int j=0; j<(int)raw->apv_fecNo->size(); j++) {
         if (raw->apv_id->at(j)==12) {
            for (int i=0; i<(int)raw->apv_q->at(j).size(); i++) {
               if (raw->apv_q->at(j).at(i) > temp) {
                  temp = raw->apv_q->at(j).at(i);
                  which_channel = raw->apv_ch->at(j);
               }
            }
         }
      }
      for (int j=0; j<(int)raw->apv_fecNo->size(); j++)
         if (raw->apv_id->at(j)==12)
            h_channel_12->Fill(which_channel, raw->apv_ch->at(j));

      for (int j=0; j<(int)raw->apv_fecNo->size(); j++) {
         if (raw->apv_id->at(j)==13) {
            for (int i=0; i<(int)raw->apv_q->at(j).size(); i++) {
               if (raw->apv_q->at(j).at(i) > temp) {
                  temp = raw->apv_q->at(j).at(i);
                  which_channel = raw->apv_ch->at(j);
               }
            }
         }
      }
      for (int j=0; j<(int)raw->apv_fecNo->size(); j++)
         if (raw->apv_id->at(j)==13)
            h_channel_13->Fill(which_channel, raw->apv_ch->at(j));

      for (int j=0; j<(int)raw->apv_fecNo->size(); j++) {
         if (raw->apv_id->at(j)==14) {
            for (int i=0; i<(int)raw->apv_q->at(j).size(); i++) {
               if (raw->apv_q->at(j).at(i) > temp) {
                  temp = raw->apv_q->at(j).at(i);
                  which_channel = raw->apv_ch->at(j);
               }
            }
         }
      }
      for (int j=0; j<(int)raw->apv_fecNo->size(); j++)
         if (raw->apv_id->at(j)==14)
            h_channel_14->Fill(which_channel, raw->apv_ch->at(j));

      for (int j=0; j<(int)raw->apv_fecNo->size(); j++) {
         if (raw->apv_id->at(j)==15) {
            for (int i=0; i<(int)raw->apv_q->at(j).size(); i++) {
               if (raw->apv_q->at(j).at(i) > temp) {
                  temp = raw->apv_q->at(j).at(i);
                  which_channel = raw->apv_ch->at(j);
               }
            }
         }
      }
      for (int j=0; j<(int)raw->apv_fecNo->size(); j++)
         if (raw->apv_id->at(j)==15)
            h_channel_15->Fill(which_channel, raw->apv_ch->at(j));

      
  } // main loop

	TCanvas *d = new TCanvas("d", "d", 1200, 1200); d->Divide(4,4);
   gPad->SetLogz(0);
   d->cd(1); h_channel_0->Draw("COLZ"); h_channel_0->GetXaxis()->SetRangeUser(1,128); h_channel_0->GetYaxis()->SetRangeUser(1,128);
	d->cd(2); h_channel_1->Draw("COLZ"); h_channel_1->GetXaxis()->SetRangeUser(1,128); h_channel_1->GetYaxis()->SetRangeUser(1,128);
	d->cd(3); h_channel_2->Draw("COLZ"); h_channel_2->GetXaxis()->SetRangeUser(1,128); h_channel_2->GetYaxis()->SetRangeUser(1,128);
	d->cd(4); h_channel_3->Draw("COLZ"); h_channel_3->GetXaxis()->SetRangeUser(1,128); h_channel_3->GetYaxis()->SetRangeUser(1,128);
	d->cd(5); h_channel_4->Draw("COLZ"); h_channel_4->GetXaxis()->SetRangeUser(1,128); h_channel_4->GetYaxis()->SetRangeUser(1,128);
	d->cd(6); h_channel_5->Draw("COLZ"); h_channel_5->GetXaxis()->SetRangeUser(1,128); h_channel_5->GetYaxis()->SetRangeUser(1,128);
	d->cd(7); h_channel_6->Draw("COLZ"); h_channel_6->GetXaxis()->SetRangeUser(1,128); h_channel_6->GetYaxis()->SetRangeUser(1,128);
	d->cd(8); h_channel_7->Draw("COLZ"); h_channel_7->GetXaxis()->SetRangeUser(1,128); h_channel_7->GetYaxis()->SetRangeUser(1,128);
   d->cd(9); h_channel_8->Draw("COLZ"); h_channel_0->GetXaxis()->SetRangeUser(1,128); h_channel_8->GetYaxis()->SetRangeUser(1,128);
   d->cd(10); h_channel_9->Draw("COLZ"); h_channel_1->GetXaxis()->SetRangeUser(1,128); h_channel_9->GetYaxis()->SetRangeUser(1,128);
   d->cd(11); h_channel_10->Draw("COLZ"); h_channel_2->GetXaxis()->SetRangeUser(1,128); h_channel_10->GetYaxis()->SetRangeUser(1,128);
   d->cd(12); h_channel_11->Draw("COLZ"); h_channel_3->GetXaxis()->SetRangeUser(1,128); h_channel_11->GetYaxis()->SetRangeUser(1,128);
   d->cd(13); h_channel_12->Draw("COLZ"); h_channel_4->GetXaxis()->SetRangeUser(1,128); h_channel_12->GetYaxis()->SetRangeUser(1,128);
   d->cd(14); h_channel_13->Draw("COLZ"); h_channel_5->GetXaxis()->SetRangeUser(1,128); h_channel_13->GetYaxis()->SetRangeUser(1,128);
   d->cd(15); h_channel_14->Draw("COLZ"); h_channel_6->GetXaxis()->SetRangeUser(1,128); h_channel_14->GetYaxis()->SetRangeUser(1,128);
   d->cd(16); h_channel_15->Draw("COLZ"); h_channel_7->GetXaxis()->SetRangeUser(1,128); h_channel_15->GetYaxis()->SetRangeUser(1,128);


  cout << endl;

//  TFile hfile1("~/Desktop/h_beam_profile1_50.root", "Recreate");
//  h_beam_profile1_50->Write();
//  hfile1.Close();





} 
