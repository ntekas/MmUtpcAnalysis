//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Jul 24 17:48:13 2014 by ROOT version 5.34/10
// from TTree clusters/clusters_tree
// found on file: run691_processed.root
//////////////////////////////////////////////////////////

#ifndef clusters_h
#define clusters_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
#include <vector>
#include <vector>
#include <vector>
#include <vector>
#include <vector>
#include <vector>

// Fixed size dimensions of array or collections stored in the TTree if any.

class clusters {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   vector<unsigned long> *cl_event;
   vector<string>  *cl_chamber_name;
   vector<string>  *cl_readout_name;
   vector<unsigned long> *cl_chamber_id;
   vector<unsigned long> *cl_plane_id;
   vector<unsigned long> *cl_readout_id;
   vector<double>  *cl_charge;
   vector<double>  *cl_time;
   vector<double>  *cl_pos_cm_strip;
   vector<double>  *cl_pos_cm_mm;
   vector<double>  *cl_pos_mid_strip;
   vector<double>  *cl_pos_mid_mm;
   vector<long>    *cl_width;
   vector<double>  *cl_track_p0;
   vector<double>  *cl_track_p1;
   vector<double>  *cl_fit_corr;
   vector<short>   *cl_fit_fail;
   vector<unsigned long> *cl_nclu;
   vector<double>  *cl_track_pos_half;
   vector<double>  *cl_pos_comb;
   vector<vector<unsigned int> > *cl_strips;
   vector<double>  *cl_qmax;
   vector<unsigned int> *cl_first_strip;
   vector<short>   *cl_first_timebin;
   vector<short>   *cl_tbqmax_spread;
   vector<short>   *cl_particle_type;
   vector<vector<unsigned int> > *st_number;
   vector<vector<short> > *st_qmax;
   vector<vector<short> > *st_tbqmax;
   vector<vector<short> > *st_tbcfd;
   vector<vector<double> > *st_qfit;
   vector<vector<double> > *st_tfit;
   vector<vector<double> > *st_tfitcfd;
   vector<vector<vector<double> > > *st_fitpars;
   vector<vector<double> > *st_qfit_fd;
   vector<vector<double> > *st_tfit_fd;
   vector<vector<double> > *st_tfitcfd_fd;
   vector<vector<double> > *st_tfitbase_fd;
   vector<vector<double> > *st_tfit_err_fd;
   vector<vector<double> > *st_tfitcfd_err_fd;
   vector<vector<double> > *st_tfitbase_err_fd;
   vector<vector<vector<double> > > *st_fitpars_fd;

   // List of branches
   TBranch        *b_cl_event;   //!
   TBranch        *b_cl_chamber_name;   //!
   TBranch        *b_cl_readout_name;   //!
   TBranch        *b_cl_chamber_id;   //!
   TBranch        *b_cl_plane_id;   //!
   TBranch        *b_cl_readout_id;   //!
   TBranch        *b_cl_charge;   //!
   TBranch        *b_cl_time;   //!
   TBranch        *b_cl_pos_cm_strip;   //!
   TBranch        *b_cl_pos_cm_mm;   //!
   TBranch        *b_cl_pos_mid_strip;   //!
   TBranch        *b_cl_pos_mid_mm;   //!
   TBranch        *b_cl_width;   //!
   TBranch        *b_cl_track_p0;   //!
   TBranch        *b_cl_track_p1;   //!
   TBranch        *b_cl_fit_corr;   //!
   TBranch        *b_cl_fit_fail;   //!
   TBranch        *b_cl_nclu;   //!
   TBranch        *b_cl_track_pos_half;   //!
   TBranch        *b_cl_pos_comb;   //!
   TBranch        *b_cl_strips;   //!
   TBranch        *b_cl_qmax;   //!
   TBranch        *b_cl_first_strip;   //!
   TBranch        *b_cl_first_timebin;   //!
   TBranch        *b_cl_tbqmax_spread;   //!
   TBranch        *b_cl_particle_type;   //!
   TBranch        *b_st_number;   //!
   TBranch        *b_st_qmax;   //!
   TBranch        *b_st_tbqmax;   //!
   TBranch        *b_st_tbcfd;   //!
   TBranch        *b_st_qfit;   //!
   TBranch        *b_st_tfit;   //!
   TBranch        *b_st_tfitcfd;   //!
   TBranch        *b_st_fitpars;   //!
   TBranch        *b_st_qfit_fd;   //!
   TBranch        *b_st_tfit_fd;   //!
   TBranch        *b_st_tfitcfd_fd;   //!
   TBranch        *b_st_tfitbase_fd;   //!
   TBranch        *b_st_tfit_err_fd;   //!
   TBranch        *b_st_tfitcfd_err_fd;   //!
   TBranch        *b_st_tfitbase_err_fd;   //!
   TBranch        *b_st_fitpars_fd;   //!

   clusters(TTree *tree=0);
   virtual ~clusters();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef clusters_cxx
clusters::clusters(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("run691_processed.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("run691_processed.root");
      }
      f->GetObject("clusters",tree);

   }
   Init(tree);
}

clusters::~clusters()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t clusters::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t clusters::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void clusters::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   cl_event = 0;
   cl_chamber_name = 0;
   cl_readout_name = 0;
   cl_chamber_id = 0;
   cl_plane_id = 0;
   cl_readout_id = 0;
   cl_charge = 0;
   cl_time = 0;
   cl_pos_cm_strip = 0;
   cl_pos_cm_mm = 0;
   cl_pos_mid_strip = 0;
   cl_pos_mid_mm = 0;
   cl_width = 0;
   cl_track_p0 = 0;
   cl_track_p1 = 0;
   cl_fit_corr = 0;
   cl_fit_fail = 0;
   cl_nclu = 0;
   cl_track_pos_half = 0;
   cl_pos_comb = 0;
   cl_strips = 0;
   cl_qmax = 0;
   cl_first_strip = 0;
   cl_first_timebin = 0;
   cl_tbqmax_spread = 0;
   cl_particle_type = 0;
   st_number = 0;
   st_qmax = 0;
   st_tbqmax = 0;
   st_tbcfd = 0;
   st_qfit = 0;
   st_tfit = 0;
   st_tfitcfd = 0;
   st_fitpars = 0;
   st_qfit_fd = 0;
   st_tfit_fd = 0;
   st_tfitcfd_fd = 0;
   st_tfitbase_fd = 0;
   st_tfit_err_fd = 0;
   st_tfitcfd_err_fd = 0;
   st_tfitbase_err_fd = 0;
   st_fitpars_fd = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("cl_event", &cl_event, &b_cl_event);
   fChain->SetBranchAddress("cl_chamber_name", &cl_chamber_name, &b_cl_chamber_name);
   fChain->SetBranchAddress("cl_readout_name", &cl_readout_name, &b_cl_readout_name);
   fChain->SetBranchAddress("cl_chamber_id", &cl_chamber_id, &b_cl_chamber_id);
   fChain->SetBranchAddress("cl_plane_id", &cl_plane_id, &b_cl_plane_id);
   fChain->SetBranchAddress("cl_readout_id", &cl_readout_id, &b_cl_readout_id);
   fChain->SetBranchAddress("cl_charge", &cl_charge, &b_cl_charge);
   fChain->SetBranchAddress("cl_time", &cl_time, &b_cl_time);
   fChain->SetBranchAddress("cl_pos_cm_strip", &cl_pos_cm_strip, &b_cl_pos_cm_strip);
   fChain->SetBranchAddress("cl_pos_cm_mm", &cl_pos_cm_mm, &b_cl_pos_cm_mm);
   fChain->SetBranchAddress("cl_pos_mid_strip", &cl_pos_mid_strip, &b_cl_pos_mid_strip);
   fChain->SetBranchAddress("cl_pos_mid_mm", &cl_pos_mid_mm, &b_cl_pos_mid_mm);
   fChain->SetBranchAddress("cl_width", &cl_width, &b_cl_width);
   fChain->SetBranchAddress("cl_track_p0", &cl_track_p0, &b_cl_track_p0);
   fChain->SetBranchAddress("cl_track_p1", &cl_track_p1, &b_cl_track_p1);
   fChain->SetBranchAddress("cl_fit_corr", &cl_fit_corr, &b_cl_fit_corr);
   fChain->SetBranchAddress("cl_fit_fail", &cl_fit_fail, &b_cl_fit_fail);
   fChain->SetBranchAddress("cl_nclu", &cl_nclu, &b_cl_nclu);
   fChain->SetBranchAddress("cl_track_pos_half", &cl_track_pos_half, &b_cl_track_pos_half);
   fChain->SetBranchAddress("cl_pos_comb", &cl_pos_comb, &b_cl_pos_comb);
   fChain->SetBranchAddress("cl_strips", &cl_strips, &b_cl_strips);
   fChain->SetBranchAddress("cl_qmax", &cl_qmax, &b_cl_qmax);
   fChain->SetBranchAddress("cl_first_strip", &cl_first_strip, &b_cl_first_strip);
   fChain->SetBranchAddress("cl_first_timebin", &cl_first_timebin, &b_cl_first_timebin);
   fChain->SetBranchAddress("cl_tbqmax_spread", &cl_tbqmax_spread, &b_cl_tbqmax_spread);
   fChain->SetBranchAddress("cl_particle_type", &cl_particle_type, &b_cl_particle_type);
   fChain->SetBranchAddress("st_number", &st_number, &b_st_number);
   fChain->SetBranchAddress("st_qmax", &st_qmax, &b_st_qmax);
   fChain->SetBranchAddress("st_tbqmax", &st_tbqmax, &b_st_tbqmax);
   fChain->SetBranchAddress("st_tbcfd", &st_tbcfd, &b_st_tbcfd);
   fChain->SetBranchAddress("st_qfit", &st_qfit, &b_st_qfit);
   fChain->SetBranchAddress("st_tfit", &st_tfit, &b_st_tfit);
   fChain->SetBranchAddress("st_tfitcfd", &st_tfitcfd, &b_st_tfitcfd);
   fChain->SetBranchAddress("st_fitpars", &st_fitpars, &b_st_fitpars);
   fChain->SetBranchAddress("st_qfit_fd", &st_qfit_fd, &b_st_qfit_fd);
   fChain->SetBranchAddress("st_tfit_fd", &st_tfit_fd, &b_st_tfit_fd);
   fChain->SetBranchAddress("st_tfitcfd_fd", &st_tfitcfd_fd, &b_st_tfitcfd_fd);
   fChain->SetBranchAddress("st_tfitbase_fd", &st_tfitbase_fd, &b_st_tfitbase_fd);
   fChain->SetBranchAddress("st_tfit_err_fd", &st_tfit_err_fd, &b_st_tfit_err_fd);
   fChain->SetBranchAddress("st_tfitcfd_err_fd", &st_tfitcfd_err_fd, &b_st_tfitcfd_err_fd);
   fChain->SetBranchAddress("st_tfitbase_err_fd", &st_tfitbase_err_fd, &b_st_tfitbase_err_fd);
   fChain->SetBranchAddress("st_fitpars_fd", &st_fitpars_fd, &b_st_fitpars_fd);
   Notify();
}

Bool_t clusters::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void clusters::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t clusters::Cut(Long64_t entry)
{
   (void)entry;
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef clusters_cxx
