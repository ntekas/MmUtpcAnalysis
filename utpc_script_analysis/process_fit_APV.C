//#define MMegasHough_cxx
//#include "MMegasHough.h"
#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
using namespace std;
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TH1D.h"
#include <TStyle.h>
#include <TCanvas.h>
#include "TROOT.h"
#include <map>
#include "TCanvas.h"
#include "TRandom.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TMarker.h"
#include "TString.h"
#include "TFile.h"
#include "TMath.h"
#include "TLine.h"
#include "TBranch.h"
#include "TTree.h"
#include "TPad.h"
#include "TChain.h"
#include "TBox.h"
#include "TMinuit.h"
#include "TSpectrum.h"
#include "TSystem.h"
#include "Riostream.h"
#include "TROOT.h"
#include "TFitResult.h"

//#include "strips.C"
//#include "raw.C"
#include "apv_raw.C"
#include "APV_RAW_PED.C"
#include "clusters.C"
#include "/Users/ntekas/atlasstyle/AtlasStyle.C"
#include "/Users/ntekas/atlasstyle/AtlasUtils.C"
#include "/Users/ntekas/atlasstyle/AtlasLabels.C"
//#include "strips_fit.C"
//==================================================================================================================================================
//==================================================================================================================================================
//==================================================================================================================================================

//Analysis for H2 Test Beam Data, June 2012,   Beam------>  DUB1/ROM__T2/T2_T3/T4__DUB2/LNF  ----_>  

//==================================================================================================================================================
//==================================================================================================================================================
//==================================================================================================================================================


//++++++++++++++++++++strip object++++++++++++++++++++++++++++++++
//strips *strip;
apv_raw *raw_tree;
APV_RAW_PED *ped_tree;
clusters *clusters_tree;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
TF1 *m_fitsnr;
vector <double> apvFitResults;
vector <double> fd_fir_pars;
//++++++++++++++++++++new ntuple + new tree++++++++++++++++++++++++++++++++
TFile *ntuple;
TFile *fit_data;
//TTree *strips;
TTree *aux;

UInt_t          evt;
vector<string>  *mm_id;
vector<unsigned int> *mm_plane;
vector<unsigned int> *mm_readout;
vector<unsigned int> *mm_strip;
vector<double>  *tfit_fd;
vector<double>  *tfit_fd_slope;
vector<double>  *tfit_err_fd;
vector<double>  *tfit_fd_chi2prob;
vector<short>   *qmax;
vector<double>   *qmax_fit;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++variables for new ntuple+++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++NEW Histos for debugging++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

TH1F *m_hist_q_vs_time = new TH1F("m_hist_q_vs_time","m_hist_q_vs_time",27,0,27);

TH1F *h_all_times = new TH1F("all_times","all_times",500,0,500);
TH2F *h_all_times_vs_charge = new TH2F("all_times_vs_charge","all_times_vs_charge",250,0,500,200,0,2000);
TH2F *h_all_times_vs_slope = new TH2F("all_times_vs_slope","all_times_vs_slope",250,0,500,300,0,3);

TF1 *f_time = new TF1("f_time","x",0,300);

TCanvas *a;

//==============================================================================================



//returns least square parameters
vector <double> lsquares_gen (vector<double> x, vector<double> z)	{//more general, works with vectors of positions
   
   double sumz=0, sumx=0, sumz2=0, xmean=0, zmean=0, sumzx=0;
   vector <double> lsquares_par;
   
   int nochambers = x.size();
   
   for(int pnt=0; pnt<nochambers; pnt++)	{
      sumz+=z.at(pnt);
      sumx+=x.at(pnt);
      sumz2+=TMath::Power(z.at(pnt),2);
      sumzx+=z.at(pnt)*(x.at(pnt));
   }
   
   xmean = sumx/nochambers;
   zmean = sumz/nochambers;
   
   double beta = (sumzx-nochambers*(xmean*zmean))/(sumz2-nochambers*TMath::Power(zmean,2));//slope x
   double alpha = (xmean*sumz2-zmean*sumzx)/(sumz2-nochambers*TMath::Power(zmean,2));//intercept x
   
   lsquares_par.push_back(beta);lsquares_par.push_back(alpha);
   
   return lsquares_par;
}


void sortStrips(vector<int> &stripPos, vector<float> &stripCharge, vector<float> &stripTime, vector<float> &stripTimeWidth)
{
   //   std::cout<<"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<std::endl;
   //   std::cout<<"Before"<<std::endl;
   //   for(int iStrip=0; iStrip<stripPos.size(); iStrip++)
   //   {
   //      std::cout<<stripPos.at(iStrip)<<" "<<stripCharge.at(iStrip)<<" "<<stripTime.at(iStrip)<<std::endl;
   //   }
   for(int iStrip=0; iStrip<stripPos.size(); iStrip++)
   {
      for(int jStrip=stripPos.size()-1; jStrip>iStrip; jStrip--)
      {
         if(stripPos.at(jStrip-1)>stripPos.at(jStrip))
         {
            swap(stripPos.at(jStrip-1),stripPos.at(jStrip));
            swap(stripCharge.at(jStrip-1),stripCharge.at(jStrip));
            swap(stripTime.at(jStrip-1),stripTime.at(jStrip));
            //swap(stripTimeWidth.at(jStrip-1),stripTimeWidth.at(jStrip));
         }
      }
   }
   //   std::cout<<"After"<<std::endl;
   //   for(int iStrip=0; iStrip<stripPos.size(); iStrip++)
   //   {
   //      std::cout<<stripPos.at(iStrip)<<" "<<stripCharge.at(iStrip)<<" "<<stripTime.at(iStrip)<<std::endl;
   //   }
   
}


double fitFunctionFinal(double *x, double *par)
{
	//double Limit=1;
	//double UpLimit = CApvChargeFitter::Maxbin+Limit, DownLimit = CApvChargeFitter::Maxbin-Limit;
	//if (x[0] <= par[1] || (x[0]>DownLimit && x[0]<UpLimit) || (x[0]<CApvChargeFitter::startBin || x[0]>CApvChargeFitter::endBin) || (x[0]==CApvChargeFitter::LeftBinOne) || (x[0]==CApvChargeFitter::LeftBinTwo) || (x[0]==CApvChargeFitter::LeftBinThree) || (x[0]==CApvChargeFitter::LeftBinFour) ||
	//(x[0]==CApvChargeFitter::RightBinOne) || (x[0]==CApvChargeFitter::RightBinTwo)/* || (x[0]==CApvChargeFitter::RightBinThree) || (x[0]==CApvChargeFitter::RightBinFour) */) { //rejecting the bins with high charge
   //if (x[0] <= par[1]) { //for four/second fit function .79, .791
   if (x[0] <= par[1] || x[0] >= 1600) { //for four/second fit function .79, .791
      TF1::RejectPoint();
      return 0;
   }
   //par[0]->a, par[1]->g, par[2]->σ, par[3]->d, par[4]->k
   return (par[0]*TMath::Power(x[0]-par[1],par[3])*TMath::Exp(-(x[0]-par[1])/(2*par[2]*par[2])));
   //return (par[0]*TMath::Power(x[0]-par[1],par[3])*TMath::Exp(-(x[0]-par[1])/(par[2])));
	//return (par[0]*TMath::Power(x[0]-par[1],par[3])*TMath::Exp(-TMath::Power((x[0]-par[1]),par[4])/(2*par[2]*par[2])));
}

TF1 *ntuafit(TH1F *his, Double_t *fitrange, Double_t *startvalues, Double_t *parlimitslo, Double_t *parlimitshi, Double_t *fitparams, Double_t *fiterrors){
   //   par[0]=a
   //   par[1]=g
   //   par[2]=s
   //   par[3]=d
   Int_t i;
   Char_t FunName[100];
	//TMinuit *gMinuit = new TMinuit(5);

   sprintf(FunName,"Fitfcn_%s",his->GetName());

   TF1 *ffitold = (TF1*)gROOT->GetListOfFunctions()->FindObject(FunName);
   if (ffitold) delete ffitold;

   TF1 *ffit = new TF1(FunName,fitFunctionFinal,fitrange[0],fitrange[1],4);
   ffit->SetParameters(startvalues);
   ffit->SetParNames("a","g","s","d");

   for (i=0; i<4; i++) {
      ffit->SetParLimits(i, parlimitslo[i], parlimitshi[i]);
   }
   ffit->SetLineColor(kGreen);
   his->Fit(FunName,"QR+");   // fit within specified range, use ParLimits, do not plot
	//if(gMinuit->fCstatu!="UNDEFINED")
	//std::cout <<"Minuit : "<<gMinuit->fCstatu<<std::endl;

   return (ffit);              // return fit function

}


vector <double> fitApvPulse(TH1F *m_hist_q_vs_time)
{
   vector <double> fitResults;
   double Maxbin=0;double Minbin=0;double startBin=0;double endBin=0;
   double QMaxHisto=0;double RightBinOne=0;double RightBinTwo=0;double RightBinThree=0;
   double RightBinFour=0;double LeftBinOne=0;double LeftBinTwo=0;double LeftBinThree=0;
   double LeftBinFour=0;
   double m_time=0.0, m_charge=0.0, m_tau=0.0, m_tzero=0.0;

   //	int failedfitswithroot = 0;
	double to,toSq;
   //	float qMaxFit=0.0;
   //	float WidthLandau=0.0;
   //	float SigmaGauss=0.0;
	double max = 0.0;
   //   , integralOfHisto=0.0;
   //	bool goodHistogram = true;

   double tau_temp=0.0;
   double mean=0.0;double std=0.0;double mean_sq=0.0;double integral_function=0.0;


	// check for plato in beggining
	/*Maxbin = m_hist_q_vs_time->GetMaximumBin();
    Minbin = m_hist_q_vs_time->GetMinimumBin();
    startBin = (m_hist_q_vs_time->GetMaximumBin())-5;
    for(int l=startBin; l<=Maxbin-3; ++l) {
    if (m_hist_q_vs_time->GetBinContent(l+1)-m_hist_q_vs_time->GetBinContent(l)<2)
    goodHistogram = false;
    }	// end of check
    */
   max=0;
	to=0;
	toSq=0;
	if (m_hist_q_vs_time->GetEntries()>0) { //problematic wires
      //		if(goodHistogram==true)	{
      Maxbin = m_hist_q_vs_time->GetMaximumBin();
      QMaxHisto = m_hist_q_vs_time->GetMaximum();
      //std::cout<<QMaxHisto<<"\n";
      /*for (int s=Maxbin; s>1; --s) {
       if (m_hist_q_vs_time->GetBinContent(s)!=0 && m_hist_q_vs_time->GetBinContent(s)>m_hist_q_vs_time->GetBinContent(s-1))
       startBin = m_hist_q_vs_time->GetBinCenter(s)-1;
       if (m_hist_q_vs_time->GetBinContent(s)==0)
       break;
       }
       for (int s=Maxbin; s<qdata.size(); ++s) {
       if (m_hist_q_vs_time->GetBinContent(s)!=0)
       endBin = m_hist_q_vs_time->GetBinCenter(s)+1;
       else
       break;
       }*/

      startBin=(Maxbin-5);
      //startBin=0;
      endBin=(Maxbin+10);

      //if(Maxbin<25) {//original value was 25

      Double_t range[2] = {startBin,endBin};
      size_t arsz = 5; //TODO: ERROR check sizes, sizes were 4, assignment out of bounds below
      Double_t startvalues[arsz], parlimitslo[arsz], parlimitshi[arsz], fitparams[arsz], fiterrors[arsz];
      range[0]=startBin; range[1]=endBin;

      Double_t QMaxFitBR = 0.0;
      TF1 *fitsnr2;

      if(QMaxHisto>=1600){
         parlimitslo[0]=10; /*.5*/parlimitslo[1]=-10.; parlimitslo[2]=0.0; parlimitslo[3]=1.0; parlimitslo[4]=0.1;//0.1;
         parlimitshi[0]=10000;/*2.*/parlimitshi[1]=20.0; parlimitshi[2]=20.0; parlimitshi[3]=15.0; parlimitshi[4]=3.0; //5.
         //               ERROR this parlimitshi[4] is never read, was out of bounds !
         startvalues[0]=QMaxHisto/12; startvalues[1]=0.0;startvalues[2]=2.0;  startvalues[3]=5.0; startvalues[4]=.5; //3.
         //
         fitsnr2 = ntuafit(m_hist_q_vs_time,range,startvalues,parlimitslo,parlimitshi,fitparams,fiterrors);
         //QMaxFitBR = fitsnr2->GetMaximum(0,30*25);
         QMaxFitBR = fitsnr2->GetMaximum(startBin,endBin);
      }

//      parlimitslo[0]=0; /*.5*/parlimitslo[1]=-5.0; parlimitslo[2]=0.0; parlimitslo[3]=1.0;//0.4
//      parlimitshi[0]=1000;/*2.*/parlimitshi[1]=30.0; parlimitshi[2]=20.0; parlimitshi[3]=10.0;//5.
//      startvalues[0]=QMaxHisto/12; startvalues[1]=0.0; startvalues[2]=5.0; startvalues[3]=2.0;//3.
      parlimitslo[0]=0; /*.5*/parlimitslo[1]=-5.0; parlimitslo[2]=0.0; parlimitslo[3]=1.0;//0.4
      parlimitshi[0]=1000;/*2.*/parlimitshi[1]=30.0; parlimitshi[2]=20.0; parlimitshi[3]=10.0;//5.
      startvalues[0]=QMaxHisto/12; startvalues[1]=0.0; startvalues[2]=5.0; startvalues[3]=2.0;//3.


      //				double chisqr;
      //				Int_t ndf;
      double BinContentLimit = 1600;
      if (m_hist_q_vs_time->GetBinContent(Maxbin+1)>BinContentLimit)
         RightBinOne = Maxbin+1;
      if (m_hist_q_vs_time->GetBinContent(Maxbin+2)>BinContentLimit)
         RightBinTwo = Maxbin+2;
      if (m_hist_q_vs_time->GetBinContent(Maxbin+3)>BinContentLimit)
         RightBinThree = Maxbin+3;
      if (m_hist_q_vs_time->GetBinContent(Maxbin+4)>BinContentLimit)
         RightBinFour = Maxbin+4;
      if (m_hist_q_vs_time->GetBinContent(Maxbin-1)>BinContentLimit)
         LeftBinOne = Maxbin-1;
      if (m_hist_q_vs_time->GetBinContent(Maxbin-2)>BinContentLimit)
         LeftBinTwo = Maxbin-2;
      if (m_hist_q_vs_time->GetBinContent(Maxbin-3)>BinContentLimit)
         LeftBinThree = Maxbin-3;
      if (m_hist_q_vs_time->GetBinContent(Maxbin-4)>BinContentLimit)
         LeftBinFour = Maxbin-4;



      m_fitsnr = ntuafit(m_hist_q_vs_time,range,startvalues,parlimitslo,parlimitshi,fitparams,fiterrors);

      if(((gMinuit->fCstatu)!="CONVERGED ") && ((gMinuit->fCstatu)!="SUCCESSFUL")
         && ((gMinuit->fCstatu)!="OK ") && ((gMinuit->fCstatu)!="OK        ")&& ((gMinuit->fCstatu)!="NOT POSDEF") && ((gMinuit->fCstatu)!="CALL LIMIT"))	{
         //if(((gMinuit->fCstatu)=="FAILED ") || ((gMinuit->fCstatu)=="FAILED")){
         //if(QMaxHisto>100)	{
         //std::cout<<gMinuit->fCstatu<<"FAILED"<<std::endl;
         range[0] = startBin+3*25; range[1] = endBin-1*25;
         m_fitsnr = ntuafit(m_hist_q_vs_time,range,startvalues,parlimitslo,parlimitshi,fitparams,fiterrors);
         //}
      }

      double a,g,s,d;
      a=m_fitsnr->GetParameter(0);
      g=m_fitsnr->GetParameter(1);//tzero information
      s=m_fitsnr->GetParameter(2);
      d=m_fitsnr->GetParameter(3);



      tau_temp = 2.*s*s;

      integral_function = a*TMath::Power(tau_temp,d+1)*TMath::Gamma(d+1);
      mean = a*TMath::Power(tau_temp,d+1)*(tau_temp*TMath::Gamma(d+2)+g*TMath::Gamma(d+1));
      //mean = mean/integral_function+g;
      mean = mean/integral_function;
      mean_sq = a*TMath::Power(tau_temp,d+1)*(tau_temp*tau_temp*TMath::Gamma(d+3)+2.*g*tau_temp*TMath::Gamma(d+2)+g*g*TMath::Gamma(d+1));
      mean_sq = mean_sq/integral_function;
      //mean_sq = mean_sq+g*g+2*g*mean;
      //        m_fitparameters[0] = a;
      //        m_fitparameters[1] = g;
      //        m_fitparameters[2] = s;
      //        m_fitparameters[3] = d;
      //k=fitsnr->GetParameter(4);
      //std::cout<<"a : "<<a<<" g : "<<g<<" s : "<<s<<" d : "<<d<<std::endl;
      //

      m_time = 25*(g+2*d*TMath::Power(s,2));//tmax without power k
      m_tzero = 25*g;
      m_tau = m_time-m_tzero;
      std = 25*TMath::Sqrt(mean_sq - mean*mean);
      //std::cout<<"Max time : "<<m_time<<" Mean : "<<mean<<" Mean_sq : "<<mean_sq<<" STD : "<<std<<std::endl;
      //m_time = g+TMath::Power(2*d/k*TMath::Power(s,2),1/k);
      m_charge = m_fitsnr->GetMaximum();//mqmax from getmax
      //        //m_charge = a*TMath::Power((2/TMath::E()),d)*TMath::Power((d*TMath::Power(s,2)),d);//qmax

   }


   fitResults.push_back(m_time);
   fitResults.push_back(m_charge);
   fitResults.push_back(m_tzero);
   fitResults.push_back(m_tau);
   fitResults.push_back(std);
   return fitResults;
}

double fitFunctionFD(double *x, double *par)
{
   return (par[0] + par[1]/(1+TMath::Exp(-(x[0]-par[2])/par[3])));
   
}

TF1 *fermiDiracfit(TH1F *his, Double_t *fitrange, Double_t *startvalues, Double_t *fitparams, Double_t *fiterrors)
{
   //   par[0]=a
   //   par[1]=g
   //   par[2]=s
   //   par[3]=d
   
   Char_t FdName[100];
   sprintf(FdName,"FitfcnFD_%s",his->GetName());
   
   TF1 *ffitold = (TF1*)gROOT->GetListOfFunctions()->FindObject(FdName);
   if (ffitold) delete ffitold;
   
   TF1 *ffit = new TF1(FdName, fitFunctionFD, 0, 8, 4);
   ffit->SetParameters(his->GetBinContent(1), his->GetMaximum(), 4, 5);
   ffit->SetParLimits(2, 0, 100);
   ffit->SetParLimits(3, 0.01, 10);
   
   ffit->SetParNames("a","g","s","d");
   
   his->Fit(FdName,"QR+", "", 0, his->GetMaximumBin()+1);   // fit within specified range, use ParLimits, do not plot
   //his->Fit(FdName,"", "", 0, his->GetMaximumBin());   // fit within specified range, use ParLimits, do not plot
   if(ffit->GetParameter(3)<.2) {
      ffit->FixParameter(0,0);
      his->Fit(FdName,"QR+", "", 0, his->GetMaximumBin()+1);
      if (ffit->GetParameter(3)<.2) {
         his->Fit(FdName,"QR+", "", 0, his->GetMaximumBin()+1);
         if (ffit->GetParameter(3)<.2) {
            ffit->SetParameter(0, 0.);
            ffit->SetParameter(1, 0.);
            ffit->SetParameter(2, 0.);
            ffit->SetParameter(3, 0.);
         }
      }
   }
   
   //	 std::cout<< ffit->GetParameter(2)*25. <<std::endl << std::endl;
   //	 std::cout<< ffit->GetParameter(2)*25. << " " << ffit->GetParError(2) <<std::endl << std::endl;
   
   return (ffit);              // return fit function
   
}

vector <double> calculate_fermiDirac(std::vector<short> data, double ped_std)
{
	//init(data.size());
   m_hist_q_vs_time->Reset();
   
   std::vector<double> m_fitparameters_fermiDirac;
   TF1 *m_fitsnr_fermiDirac;
   Double_t m_time;
   Double_t m_time_fermiDirac;
   Double_t m_charge;
   Double_t m_charge_fermiDirac;
   Double_t m_time_error_fermiDirac;
   Double_t m_time_error_fermiDirac_cfd;
   Double_t m_time_error_fermiDirac_base;
   bool m_fail_flag;
   bool m_fail_flag_fermiDirac;

   double Maxbin, Minbin, startBin, endBin, QMaxHisto, RightBinOne, RightBinTwo, RightBinThree, RightBinFour, LeftBinOne, LeftBinTwo, LeftBinThree, LeftBinFour;

   TRandom r;

	size_t qsize=data.size();
	double charge_error;
	for(size_t itb=0; itb<qsize; ++itb)	{
		//m_hist_q_vs_time->Fill(itb, data.at(itb));
      //m_hist_q_vs_time->SetBinContent(itb+1, r.Gaus(data.at(itb),20));//simulate the strip capacitance noise
      m_hist_q_vs_time->SetBinContent(itb+1, data.at(itb));

      //		if(data.at(itb)!=0)
      //			charge_error = 1/TMath::Abs(data.at(itb));
      //		else
      //charge_error = 10.;
      charge_error = ped_std;
		m_hist_q_vs_time->SetBinError(itb+1, charge_error);
      m_hist_q_vs_time->SetDirectory(0);
	}
   //apvFitResults=fitApvPulse(m_hist_q_vs_time);

	double to,toSq;
	double max = 0.0;
   
	// check for plato in beggining
   max=0;
	to=0;
	toSq=0;
	if (m_hist_q_vs_time->GetEntries()>0) { //problematic wires
      //		if(goodHistogram==true)	{
      Maxbin = m_hist_q_vs_time->GetMaximumBin();
      QMaxHisto = m_hist_q_vs_time->GetMaximum();
      
      startBin=Maxbin-5;
      endBin=Maxbin+10;
      
      Double_t range[2] = {startBin,endBin};
      size_t arsz = 5; //TODO: ERROR check sizes, sizes were 4, assignment out of bounds below
      Double_t startvalues[arsz], fitparams[arsz], fiterrors[arsz];
      range[0]=startBin; range[1]=endBin;
      
      Double_t QMaxFitBR = 0.0;
      TF1 *fitsnr2;
      
      startvalues[0]=QMaxHisto/12; startvalues[1]=0.0; startvalues[2]=5.0; startvalues[3]=2.0;//3.
      
      //			Int_t ndf;
      double BinContentLimit = 1600;

      m_fitsnr_fermiDirac = fermiDiracfit(m_hist_q_vs_time,range,startvalues,fitparams,fiterrors);
      
      if(((gMinuit->fCstatu)!="CONVERGED ") && ((gMinuit->fCstatu)!="SUCCESSFUL")
         && ((gMinuit->fCstatu)!="OK ") && ((gMinuit->fCstatu)!="CALL LIMIT"))	{
         m_fitsnr_fermiDirac = 0;
         m_fitsnr_fermiDirac = fermiDiracfit(m_hist_q_vs_time,range,startvalues,fitparams,fiterrors);
      }
      
      double p1x,p2x,p3x,p1y,p2y,p3y;
      vector<double> px,py;
      vector<double> lsq;
      p1y = 0.9*m_fitsnr_fermiDirac->GetMaximum();p1x = m_fitsnr_fermiDirac->GetX(p1y);
      p2y = 0.5*m_fitsnr_fermiDirac->GetMaximum();p2x = m_fitsnr_fermiDirac->GetX(p2y);
      p3y = 0.1*m_fitsnr_fermiDirac->GetMaximum();p3x = m_fitsnr_fermiDirac->GetX(p3y);
      px.push_back(p1x);px.push_back(p2x);px.push_back(p3x);
      py.push_back(p1y);py.push_back(p2y);py.push_back(p3y);

      lsq = lsquares_gen(py,px);

      double a,g,s,d;
      //double prob_chi2;
      g=m_fitsnr_fermiDirac->GetParameter(1);//tzero information
      s=m_fitsnr_fermiDirac->GetParameter(2)*25;//fermi dirac fit time
      //s=25*(m_fitsnr_fermiDirac->GetMinimum()-lsq.at(1))/lsq.at(0);//time from linear fit in 3 points of fermi dirac and calculate base
      a=m_fitsnr_fermiDirac->GetParError(2)*25;
      d=m_fitsnr_fermiDirac->GetParameter(3)*25;
      //prob_chi2 = TMath::Prob(m_fitsnr_fermiDirac->GetChisquare(),m_fitsnr_fermiDirac->GetNDF());

      m_fitparameters_fermiDirac.push_back(s);//fermi dirac fit time
      //m_fitparameters_fermiDirac.push_back(Maxbin*25);//max time bin time
      m_fitparameters_fermiDirac.push_back(a);
      m_fitparameters_fermiDirac.push_back(g);
      m_fitparameters_fermiDirac.push_back(d);
      m_fitparameters_fermiDirac.push_back(QMaxHisto);
      m_fitparameters_fermiDirac.push_back(m_fitsnr_fermiDirac->GetParameter(0)*25);
      m_fitparameters_fermiDirac.push_back(TMath::Prob(m_fitsnr_fermiDirac->GetChisquare(),m_fitsnr_fermiDirac->GetNDF()));
      //m_fitparameters_fermiDirac.push_back(0);
      //m_charge_fermiDirac = m_fitsnr_fermiDirac->Eval(s);
      //m_time_fermiDirac = s;
      //m_time_error_fermiDirac = m_fitsnr_fermiDirac->GetParError(2);
      

      
//      if(m_fitsnr_fermiDirac->GetParameter(2)!=0. && m_fitsnr_fermiDirac->GetParameter(3)!=0.) {
//         m_time_error_fermiDirac_cfd = TMath::Sqrt(TMath::Power(m_fitsnr_fermiDirac->GetParError(1),2)+2*TMath::Power((m_fitsnr_fermiDirac->GetParError(3)/m_fitsnr_fermiDirac->GetParameter(3)),2)+4*TMath::Power((m_fitsnr_fermiDirac->GetParError(2)/m_fitsnr_fermiDirac->GetParameter(2)),2)) ;
//      }
      
//      if(m_fitsnr_fermiDirac->GetParameter(3)!=0) {
//         m_time_error_fermiDirac_base = TMath::Sqrt(TMath::Power(m_fitsnr_fermiDirac->GetParError(2),2) + TMath::Power(m_fitsnr_fermiDirac->GetParError(3)/m_fitsnr_fermiDirac->GetParameter(3),2)/TMath::Power(2,2));
//      }
      //        //m_time = g+TMath::Power(2*d/k*TMath::Power(s,2),1/k);
      //m_charge_fermiDirac = m_fitsnr_fermiDirac->GetMaximum(0,data.size());//mqmax from getmax
      //        //m_charge = a*TMath::Power((2/TMath::E()),d)*TMath::Power((d*TMath::Power(s,2)),d);//qmax
      //        //			m_tzero=g;
      
      if(((gMinuit->fCstatu)!="CONVERGED ") && ((gMinuit->fCstatu)!="SUCCESSFUL") && ((gMinuit->fCstatu)!="OK ") && ((gMinuit->fCstatu)!="CALL LIMIT")) {
         m_charge_fermiDirac=-1000;
         m_time_fermiDirac=-1000;
         //m_fail_flag = true;
      }
      
      
      RightBinOne=0;
      RightBinTwo=0;
      RightBinThree=0;
      RightBinFour=0;
      LeftBinOne=0;
      LeftBinTwo=0;
      LeftBinThree=0;
      LeftBinFour=0;
      

   }//histo has entries
   //return apvFitResults;
   return m_fitparameters_fermiDirac;
}


void load_tree_objects(const char *  file)	{
   
   //Get file
   TFile *root_file = new TFile(file,"READ");
   
   //Load all cluster trees from file
   //strip  = new strips((TTree*)root_file->Get("strips"));

   raw_tree = new apv_raw((TTree*)root_file->Get("raw"));
   ped_tree = new APV_RAW_PED((TTree*)root_file->Get("pedestals"));
   if(std::string(file).find("processed")!=-1)//root file is processed with recomm
      clusters_tree = new clusters((TTree*)root_file->Get("clusters"));
}


void write_fit_ntuple(string file, int debug, TString chambername, bool varThresh)
{
   
   gROOT->LoadMacro("/Users/ntekas/atlasstyle/AtlasStyle.C");
   gROOT->LoadMacro("/Users/ntekas/atlasstyle/AtlasUtils.C");
   gROOT->LoadMacro("/Users/ntekas/atlasstyle/AtlasLabels.C");
   SetAtlasStyle();

   load_tree_objects(file.c_str());

   //std::string ss;
   //ss >> file;
   std::size_t found_run = file.find("run");
   std::size_t found_dotroot = file.find(".root");
   std::string run_name = file.substr(found_run,found_dotroot-found_run);
   std::string path = file.substr(0,found_run);
   std::string new_file;

   if(varThresh)
      new_file = path+run_name+"_fit_varthresh.root";
   else
      new_file = path+run_name+"_fit.root";

   //std::string new_file = path+run_name+"_fit_varthresh.root";
   fit_data = new TFile(new_file.c_str(), "RECREATE");

   int raw_evet, cl_evt;
   if (debug) a = new TCanvas("a","a");
   //fit_data = new TFile(Form("fit_clusters_%s",file), "RECREATE");
   TTree *strips_cluster;
   TTree *strips_fit = new TTree("strips_fit", "strips_fit");
   int nentries;
   int nentries_raw = (raw_tree->fChain)->GetEntries();

   nentries = nentries_raw;
   if(file.find("processed")!=-1){//root file is processed with recomm
      strips_cluster = (clusters_tree->fChain)->CloneTree(0);//clone tree
      int nentries_cluster = (clusters_tree->fChain)->GetEntries();
      if(nentries_cluster<nentries_raw)
         nentries=nentries_cluster;
      else
         nentries=nentries_raw;
   }

   strips_fit->Branch("evt", &evt);
   strips_fit->Branch("mm_id", &mm_id);
   //   strips->Branch("mm_plane", &mm_plane);
   strips_fit->Branch("mm_readout", &mm_readout);
   strips_fit->Branch("mm_strip", &mm_strip);
   strips_fit->Branch("tfit_fd", &tfit_fd);
   strips_fit->Branch("tfit_fd_slope", &tfit_fd_slope);
   strips_fit->Branch("tfit_fd_err", &tfit_err_fd);
   strips_fit->Branch("tfit_fd_chi2prob", &tfit_fd_chi2prob);
   strips_fit->Branch("qmax", &qmax);
   strips_fit->Branch("qmax_fit", &qmax_fit);
   
   std::vector <double> charge_fit_pars;
   
   std::cout<<"Getting Fitting Charge for T chambers "<<std::endl;

   ped_tree->GetEntry(0);

   double temp_ped_std=0;
   double temp_max_charge=0;
   
   m_hist_q_vs_time->GetXaxis()->SetTitle("APV Time Sample [25ns]");
   m_hist_q_vs_time->GetYaxis()->SetTitle("Charge [ADC]");
   
   //nentries = 6000;
   for (int iEntry=0, iEntryCluster=0; iEntry < nentries; iEntry++, iEntryCluster++) {
      mm_readout->clear();mm_id->clear();mm_strip->clear();tfit_fd->clear();tfit_fd_slope->clear();tfit_err_fd->clear();tfit_fd_chi2prob->clear();qmax->clear();qmax_fit->clear();
      
      //(strip->fChain)->GetEntry(iEntry);
      (raw_tree->fChain)->GetEntry(iEntry);
      if(file.find("processed")!=-1)
         (clusters_tree->fChain)->GetEntry(iEntry);
      evt = raw_tree->apv_evt;
      if (iEntry % 2000 == 0 && iEntry!=0)
         cout << "====>>>>>> " << iEntry << " processed so far <<<<<<==== \n";
      for(int num=0;num<(int)raw_tree->mm_strip->size();num++)	{
         //if(raw_tree->mm_id->at(num)!="Tmm2" && raw_tree->mm_id->at(num)!="Tmm3" && raw_tree->mm_id->at(num)!="Tmm5" && raw_tree->mm_id->at(num)!="Tmm6"/* && raw_tree->mm_id->at(num)!="T5" && raw_tree->mm_id->at(num)!="T6" && raw_tree->mm_id->at(num)!="T7" && raw_tree->mm_id->at(num)!="T8"*/) continue;
         for(int i_ped=0; i_ped<ped_tree->mm_strip->size();i_ped++)
         {
            if(raw_tree->mm_id->at(num) == ped_tree->mm_id->at(i_ped) && raw_tree->mm_readout->at(num) == ped_tree->mm_readout->at(i_ped) && raw_tree->mm_strip->at(num) == ped_tree->mm_strip->at(i_ped)){
               temp_ped_std = ped_tree->apv_pedstd->at(i_ped);
               i_ped = ped_tree->mm_strip->size()-1;
            }
         }
         
         //apply strip charge cut based on pedestal std dev
         temp_max_charge = raw_tree->apv_q->at(num).at(std::distance(std::begin(raw_tree->apv_q->at(num)),std::max_element(std::begin(raw_tree->apv_q->at(num)),std::end(raw_tree->apv_q->at(num)))));

         if(varThresh)  {
            if(6*temp_ped_std>temp_max_charge)
               continue;
         }

         charge_fit_pars = calculate_fermiDirac(raw_tree->apv_q->at(num),temp_ped_std);
         
         if(debug==1){
            if((chambername!="" && raw_tree->mm_id->at(num)==chambername) || (chambername==""))  {
//            if(charge_fit_pars.at(3) <5/* || 8*temp_ped_std>temp_max_charge*/){
//              if(charge_fit_pars.at(4)>500) {
               std::cout<<"std "<<temp_ped_std<<"max "<<temp_max_charge<<std::endl;
               a->cd();
               gStyle->SetOptFit(00000000);
               gStyle->SetOptStat(00000000);
               m_hist_q_vs_time->Draw();
               std::cout<<charge_fit_pars.at(6)<<std::endl;
               a->Update();
               //getchar();
               gPad->WaitPrimitive();
//              }
//              }
            }
         }

         if(debug==2)   {
            std::cout<<"debug "<<debug<<std::endl;
         }
         mm_readout->push_back(raw_tree->mm_readout->at(num));
         mm_id->push_back(raw_tree->mm_id->at(num));
         //mm_plane->push_back(raw_tree->mm_plane->at(num));
         mm_strip->push_back(raw_tree->mm_strip->at(num));
         tfit_fd->push_back(charge_fit_pars.at(0));
         tfit_fd_slope->push_back(charge_fit_pars.at(3));
         tfit_err_fd->push_back(charge_fit_pars.at(1));
         tfit_fd_chi2prob->push_back(charge_fit_pars.at(6));
         qmax->push_back(charge_fit_pars.at(4));
         qmax_fit->push_back(charge_fit_pars.at(5));
      }
      if(debug==0){
         if(file.find("processed")!=-1){
            if(clusters_tree->cl_event->size()!=0) {
               strips_cluster->Fill();
               strips_fit->Fill();
            }
         }
         else
            strips_fit->Fill();
      }
   }
   if(debug==0){
      if(file.find("processed")!=-1){
         strips_cluster->Write();
      }
      fit_data->Write();
      fit_data->Close();
   }

   if(debug==2)   {
      std::cout<<"debug "<<debug<<std::endl;
   }
}



