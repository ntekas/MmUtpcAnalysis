//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Apr 29 12:09:52 2013 by ROOT version 5.34/02
// from TTree strips_fit/strips_fit
// found on file: fit_run7286_processed.root
//////////////////////////////////////////////////////////

#ifndef strips_fit_h
#define strips_fit_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
#include <vector>
#include <vector>
#include <vector>

// Fixed size dimensions of array or collections stored in the TTree if any.

class strips_fit {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   UInt_t          evt;
   vector<string>  *mm_id;
   vector<unsigned int> *mm_readout;
   vector<unsigned int> *mm_strip;
   vector<double>  *tfit_fd;
   vector<double>  *tfit_fd_err;
   vector<short>   *qmax;

   // List of branches
   TBranch        *b_evt;   //!
   TBranch        *b_mm_id;   //!
   TBranch        *b_mm_readout;   //!
   TBranch        *b_mm_strip;   //!
   TBranch        *b_tfit_fd;   //!
   TBranch        *b_tfit_fd_err;   //!
   TBranch        *b_qmax;   //!

   strips_fit(TTree *tree=0);
   virtual ~strips_fit();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef strips_fit_cxx
strips_fit::strips_fit(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("fit_run7286_processed.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("fit_run7286_processed.root");
      }
      f->GetObject("strips_fit",tree);

   }
   Init(tree);
}

strips_fit::~strips_fit()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t strips_fit::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t strips_fit::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void strips_fit::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   mm_id = 0;
   mm_readout = 0;
   mm_strip = 0;
   tfit_fd = 0;
   tfit_fd_err = 0;
   qmax = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("evt", &evt, &b_evt);
   fChain->SetBranchAddress("mm_id", &mm_id, &b_mm_id);
   fChain->SetBranchAddress("mm_readout", &mm_readout, &b_mm_readout);
   fChain->SetBranchAddress("mm_strip", &mm_strip, &b_mm_strip);
   fChain->SetBranchAddress("tfit_fd", &tfit_fd, &b_tfit_fd);
   fChain->SetBranchAddress("tfit_fd_err", &tfit_fd_err, &b_tfit_fd_err);
   fChain->SetBranchAddress("qmax", &qmax, &b_qmax);
   Notify();
}

Bool_t strips_fit::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void strips_fit::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t strips_fit::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef strips_fit_cxx
